﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using Emgu.CV.ML;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.IO;

namespace SPI_AOI.Utils
{
    class FOVOptimizeV2
    {
        public bool Debug { get; set; }
        private double DistanceSmallest { get; set; }
        private Point[] BestDirection { get; set; }
        private string DebugPath = "debug/OptimizeFOV2";
        public Point[] GetAnchorsFOVOptimize(Image<Gray, byte> ImgInput, Rectangle ROI, Size FOVSRC, START_POINT StPoint = START_POINT.TOP_LEFT)
        {
            Size FOV = new Size(FOVSRC.Width - 40, FOVSRC.Height - 40);
            if (this.Debug)
            {
               if(!Directory.Exists(this.DebugPath))
               {
                    Directory.CreateDirectory(this.DebugPath);
               }
                string[] files = Directory.GetFiles(this.DebugPath);
                foreach (var item in files)
                {
                    try
                    {
                        File.Delete(item);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
            }
            Point[] anchorFOVs = new Point[0];
            if(ROI == null)
            {
                ROI = new Rectangle();
            }
            ImgInput.ROI = ROI;
            using (Image<Gray, byte> img = ImgInput.Copy())
            {
                if (this.Debug)
                {
                    CvInvoke.Imwrite($"{this.DebugPath}/imageROI.jpg", img);
                }
                ImgInput.ROI = new Rectangle();
                
                CvInvoke.Threshold(img, img, 0, 255, Emgu.CV.CvEnum.ThresholdType.Otsu);
                var anchors = GetAnchorOptimize(img, FOV);
                if (this.Debug)
                {
                    using (Image<Gray, byte> imgDraw = img.Copy())
                    {
                        for (int i = 0; i < anchors.Length; i++)
                        {
                            CvInvoke.Rectangle(imgDraw, GetRectangleByAnchor(anchors[i], FOV), new MCvScalar(127), 3);
                        }
                        CvInvoke.Imwrite($"{this.DebugPath}/imageFindAnchor.jpg", imgDraw);
                    }
                }
                for (int i = 0; i < anchors.Length; i++)
                {
                    anchors[i].X += ROI.X;
                    anchors[i].Y += ROI.Y;
                }
                var sortedByRows = SortedByRows(ImgInput, anchors, FOV);
                var sortedByCols = SortedByCols(sortedByRows);
                Point[] anchorsSorted = GetAnchorOptimizeSorted(sortedByCols, StPoint);
                if (this.Debug)
                {
                    var imageDiagram = GetDiagram(ImgInput, anchorsSorted, FOV);
                    CvInvoke.Imwrite($"{this.DebugPath}/imageDiagram.jpg", imageDiagram);
                }
                anchorFOVs = anchorsSorted;
            }
            ImgInput.ROI = new Rectangle();
            return anchorFOVs;
        }
        public Rectangle GetRectangleByAnchor(Point Anchor, Size FOV)
        {
            int x = Anchor.X - FOV.Width / 2;
            int y = Anchor.Y - FOV.Height / 2;
            int w = FOV.Width;
            int h = FOV.Height;
            return new Rectangle(x, y, w, h);
        }
        public Image<Bgr, byte> GetDiagram(Image<Gray, byte> ImgGerber, Point[] Anchors, Size FOV)
        {
            Image<Bgr, byte> imgbgr = ImgGerber.Convert<Bgr, byte>();
            for (int i = 0; i < Anchors.Length; i++)
            {
                MCvScalar[] color = new MCvScalar[] {
                    new MCvScalar(255,0,0),
                    new MCvScalar(255,255,0),
                    new MCvScalar(0,255,0),
                    new MCvScalar(0,255,255),
                    new MCvScalar(255,0,255)
                };
                Rectangle fov = new Rectangle(Anchors[i].X - FOV.Width / 2, Anchors[i].Y - FOV.Height / 2, FOV.Width, FOV.Height);
                CvInvoke.Rectangle(imgbgr, fov, color[i % 5], 2);
                CvInvoke.PutText(imgbgr, (i + 1).ToString(), Anchors[i], Emgu.CV.CvEnum.FontFace.HersheyComplex, 3, new MCvScalar(0, 255, 0), 1);
                if (i > 0)
                {
                    CvInvoke.Line(imgbgr, Anchors[i - 1], Anchors[i], new MCvScalar(0, 0, 255), 5);
                }
            }
            return imgbgr;
        }
        public Image<Bgr, byte> GetDiagramHightLight(Image<Gray, byte> ImgGerber, Point[] Anchors, Size FOV, int IndexHightLight)
        {
            Image<Bgr, byte> imgbgr = ImgGerber.Convert<Bgr, byte>();
            Rectangle highLight = Rectangle.Empty;
            for (int i = 0; i < Anchors.Length; i++)
            {
                MCvScalar colorHightLight = new MCvScalar(0, 255, 255);
                MCvScalar colorNormal = new MCvScalar(255, 0, 0);
                Rectangle fov = new Rectangle(Anchors[i].X - FOV.Width / 2, Anchors[i].Y - FOV.Height / 2, FOV.Width, FOV.Height);
                if (i == IndexHightLight)
                {
                    highLight = fov;
                }
                else
                {
                    CvInvoke.Rectangle(imgbgr, fov, new MCvScalar(255, 0, 0), 5);
                }
                CvInvoke.PutText(imgbgr, (i).ToString(), Anchors[i], Emgu.CV.CvEnum.FontFace.HersheyComplex, 3, new MCvScalar(0, 255, 0), 3);
                if (i > 0)
                {
                    CvInvoke.Line(imgbgr, Anchors[i - 1], Anchors[i], new MCvScalar(0, 0, 255), 5);
                }
            }
            if (highLight != Rectangle.Empty)
            {
                CvInvoke.Rectangle(imgbgr, highLight, new MCvScalar(0, 255, 255), 12);
            }
            return imgbgr;
        }
        private Point[] GetAnchorOptimize(Image<Gray, byte> img, Size FOV)
        {
            
            List<Point> anchors = new List<Point>();
            Rectangle boudingPad = GetExtremeBouding(img);
            if (boudingPad != Rectangle.Empty)
            {
                using (Image<Gray, byte> imageCopy = img.Copy())
                using (VectorOfVectorOfPoint contoursImageCopy = new VectorOfVectorOfPoint())
                {
                    CvInvoke.FindContours(imageCopy, contoursImageCopy, null, Emgu.CV.CvEnum.RetrType.External, Emgu.CV.CvEnum.ChainApproxMethod.ChainApproxSimple);
                    int stepH = boudingPad.Height % FOV.Height == 0 ? boudingPad.Height / FOV.Height : boudingPad.Height / FOV.Height + 1;
                    int stepW = boudingPad.Width % FOV.Width == 0 ? boudingPad.Width / FOV.Width : boudingPad.Width / FOV.Width + 1;
                    int maxFOV = stepH * stepW;
                    int countFOV = 0;
                    // limit calculation
                    Stopwatch sw = new Stopwatch();
                    sw.Start();
                    while (countFOV < maxFOV * 2)
                    {
                        Rectangle extTopLeft = GetExtremeBouding(imageCopy);
                        if (extTopLeft == Rectangle.Empty)
                            break;
                        Rectangle ROITopLeft = new Rectangle(
                            extTopLeft.X,
                            extTopLeft.Y,
                            extTopLeft.Width,
                            FOV.Height);
                        imageCopy.ROI = ROITopLeft;
                        using (Image<Gray, byte> imageROI = imageCopy.Copy())
                        {
                            imageCopy.ROI = Rectangle.Empty;
                            using (VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint())
                            {
                                CvInvoke.FindContours(imageROI, contours, null, Emgu.CV.CvEnum.RetrType.External, Emgu.CV.CvEnum.ChainApproxMethod.ChainApproxSimple);
                                while (true)
                                {
                                
                                    Rectangle extTopLeftROI = GetExtremeBouding(imageROI);
                                    if (extTopLeftROI == Rectangle.Empty)
                                        break;
                                    Rectangle fovFound = new Rectangle(
                                            extTopLeftROI.X, extTopLeftROI.Y,
                                            FOV.Width, FOV.Height);
                                
                                    
                                    for (int i = 0; i < contours.Size; i++)
                                    {
                                        Rectangle boundItem = CvInvoke.BoundingRectangle(contours[i]);
                                        if(fovFound.Contains(boundItem))
                                            CvInvoke.DrawContours(imageROI, contours, i, new MCvScalar(0), -1);
                                    }
                                    fovFound.X += ROITopLeft.X;
                                    fovFound.Y += ROITopLeft.Y;
                                    List<Point> pointCntInRect = new List<Point>();
                                    for (int i = 0; i < contoursImageCopy.Size; i++)
                                    {
                                        Rectangle boundItem = CvInvoke.BoundingRectangle(contoursImageCopy[i]);
                                        if (fovFound.Contains(boundItem))
                                        {
                                            CvInvoke.DrawContours(imageCopy, contoursImageCopy, i, new MCvScalar(0), -1);
                                            pointCntInRect.AddRange(contoursImageCopy[i].ToArray());
                                        }
                                    }
                                    Rectangle boundAllPadInFOV = GetExtremeBouding(pointCntInRect.ToArray());
                                    anchors.Add(new Point(boundAllPadInFOV.X + boundAllPadInFOV.Width / 2, boundAllPadInFOV.Y + boundAllPadInFOV.Height / 2));
                                    countFOV++;
                                }
                            }
                        }
                    }

                    Console.WriteLine(sw.ElapsedMilliseconds);
                }
                // adjust all anchors
                for (int i = 0; i < anchors.Count; i++)
                {
                    var item = anchors[i];
                    if (item.X - FOV.Width / 2 < boudingPad.X)
                    {
                        item.X = boudingPad.X + FOV.Width / 2;
                    }
                    if (item.Y - FOV.Height / 2 < boudingPad.Y)
                    {
                        item.Y = boudingPad.Y + FOV.Height / 2;
                    }
                    if (item.X + FOV.Width / 2 > boudingPad.X + boudingPad.Width)
                    {
                        item.X = boudingPad.X + boudingPad.Width - FOV.Width / 2;
                    }
                    if (item.Y + FOV.Height / 2 > boudingPad.Y + boudingPad.Height)
                    {
                        item.Y = boudingPad.Y + boudingPad.Height - FOV.Height / 2;
                    }
                    anchors[i] = item;
                }
            }
            return anchors.ToArray();
        }

        public Point[] OptimalDirection(Point[] Anchors, Size FOV, START_POINT StPoint)
        {
            Rectangle boundAnchor = GetExtremeBouding(Anchors);
            //Point stPoint = new Point();
            switch (StPoint)
            {
                case START_POINT.TOP_LEFT:
                    break;
                case START_POINT.TOP_RIGHT:
                    break;
                case START_POINT.BOT_LEFT:
                    break;
                case START_POINT.BOT_RIGHT:
                    break;
                default:
                    break;

            }
            double minDist = 0;
            List<Point> passPoint = new List<Point>();
            List<Point> restPoint = new List<Point>();
            passPoint.Add(Anchors[0]);
            for (int i = 0; i < Anchors.Length - 1; i++)
            {
                double dis = Distance(Anchors[i], Anchors[i + 1]);
                minDist += dis;
                restPoint.Add(Anchors[i + 1]);
            }
            this.DistanceSmallest = minDist;
            this.BestDirection = new Point[Anchors.Length];
            Anchors.CopyTo(this.BestDirection, 0);
            return this.BestDirection;
        }
        private void FindBack(List<Point> PassPoint, List<Point> RestPoint, int index)
        {
            
        }
        public List<Point[]> SortedByRows(Image<Gray, byte> image, Point[] Anchors, Size FOV)
        {
            int div = 3;
            List<Point[]> sorted = new List<Point[]>();
            Rectangle boundAnchor = GetExtremeBouding(Anchors);
            boundAnchor.Inflate(FOV.Width / 2, FOV.Height / 2);
            int step = boundAnchor.Height % (FOV.Height/ div) == 0 ? boundAnchor.Height / (FOV.Height / div) : boundAnchor.Height / (FOV.Height / div) + 1;
            for (int st = 0; st < step; st++)
            {
                List<Point> pointInARow = new List<Point>();
                int lsl = boundAnchor.Y + st * (FOV.Height / div);
                int usl = boundAnchor.Y + (st + 1) * (FOV.Height / div);
                for (int i = 0; i < Anchors.Length; i++)
                {
                    if (Anchors[i].Y >= lsl && Anchors[i].Y < usl)
                    {
                        pointInARow.Add(Anchors[i]);
                    }
                }
                if(pointInARow.Count > 0)
                    sorted.Add(pointInARow.ToArray());
            }
            return sorted;
        }
        public List<Point[]> SortedByCols(List<Point[]> SortedbyRows)
        {
            List<Point[]> sortedByCols = new List<Point[]>();
            for (int i = 0; i < SortedbyRows.Count; i++)
            {
                var row = SortedbyRows[i];
                sortedByCols.Add(row.OrderBy(item => item.X).ToArray());
            }
            return sortedByCols;
        }
        public Point[] GetAnchorOptimizeSorted(List<Point[]> Anchors, START_POINT StPoint)
        {
            ArrayList anchorsSorted = new ArrayList();
            /*  x = true ====>   , x = false <=====
                y = true ||         y = false /||\
                         ||                    ||
                        \||/                   ||
            */
            bool directX = true;
            bool directY = true;
            switch (StPoint)
            {
                case START_POINT.TOP_LEFT:
                    directX = true;
                    directY = true;
                    break;
                case START_POINT.BOT_LEFT:
                    directX = true;
                    directY = false;
                    break;
                case START_POINT.TOP_RIGHT:
                    directX = false;
                    directY = true;
                    break;
                case START_POINT.BOT_RIGHT:
                    directX = false;
                    directY = false;
                    break;
                default:
                    break;
            }
            for (int y = 0; y < Anchors.Count; y++)
            {
                int realValY = directY ? y : Anchors.Count - y - 1;
                for (int x = 0; x < Anchors[realValY].Length; x++)
                {
                    int realValX = directX ? x : Anchors[realValY].Length - x - 1;
                    anchorsSorted.Add(Anchors[realValY][realValX]);
                }
                if (Anchors[realValY].Length > 0)
                {
                    directX = !directX;
                }
            }
            Point[] anchors = (Point[])anchorsSorted.ToArray(typeof(Point));
            return anchors;
        }
        public double Distance(Point A, Point B)
        {
            return Math.Sqrt(Math.Pow(A.X - B.X, 2) + Math.Pow(A.Y - B.Y, 2));
        }
        public Rectangle GetExtremeBouding(Image<Gray, byte> image, List<Rectangle> RectRemove = null)
        {
            Rectangle bounding = Rectangle.Empty;
            int left = (int)Math.Pow(2,30);
            int top = (int)Math.Pow(2, 30);
            int bot = 0;
            int right = 0;
            using (VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint())
            {
                CvInvoke.FindContours(image, contours, null, Emgu.CV.CvEnum.RetrType.External, Emgu.CV.CvEnum.ChainApproxMethod.ChainApproxSimple);
                if(contours.Size > 0)
                {
                    for (int i = 0; i < contours.Size; i++)
                    {
                        Rectangle bound = CvInvoke.BoundingRectangle(contours[i]);
                        if(RectRemove != null)
                        {
                            if(RectRemove.Contains(bound))
                            {
                                continue;
                            }  
                        }
                        left = left > bound.X ? bound.X : left;
                        top = top > bound.Y ? bound.Y : top;
                        right = right < bound.X + bound.Width ? bound.X + bound.Width : right;
                        bot = bot < bound.Y + bound.Height ? bound.Y + bound.Height : bot;
                    }
                    bounding = new Rectangle(left, top, right - left, bot - top);
                }
            }
            return bounding;
        }
        public Rectangle GetExtremeBouding(Point[] PointArr)
        {
            Rectangle bounding = Rectangle.Empty;
            int left = (int)Math.Pow(2, 30);
            int top = (int)Math.Pow(2, 30);
            int bot = 0;
            int right = 0;
            if (PointArr.Length > 0)
            {
                for (int i = 0; i < PointArr.Length; i++)
                {
                    left = left > PointArr[i].X ? PointArr[i].X : left;
                    top = top > PointArr[i].Y ? PointArr[i].Y : top;
                    right = right < PointArr[i].X ? PointArr[i].X: right;
                    bot = bot < PointArr[i].Y ? PointArr[i].Y : bot;
                }
                bounding = new Rectangle(left, top, right - left, bot - top);
            }
            return bounding;
        }
    }

    public enum START_POINT
    {
        TOP_LEFT,
        TOP_RIGHT,
        BOT_LEFT,
        BOT_RIGHT
    }
}
