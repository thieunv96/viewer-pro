﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Emgu.CV;
using Emgu.CV.Structure;
using SPI_AOI.Modelsv2;

namespace Heal
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        MongoDB mMongo = MongoDB.GetInstance();
        List<Results.PanelResult> mPanelResults = new List<Results.PanelResult>();
        public MainWindow()
        {
            InitializeComponent();
            LoadUI();
            this.DataContext = this;
            dgvPanelResult.Items.Clear();
            dgvPanelResult.ItemsSource = mPanelResults;
        }

        private void Window_Initialized(object sender, EventArgs e)
        {
            mMongo.Connect();
        }
        private void LoadUI()
        {
            LoadModels();
        }
        private void LoadModels()
        {
            string selected = Convert.ToString(cbModelName.SelectedItem);
            cbModelName.Items.Clear();
            string[] modelNames = Model.GetModelNames();
            if (modelNames != null)
            {
                cbModelName.Items.Add("All");
                for (int i = 0; i < modelNames.Length; i++)
                {
                    cbModelName.Items.Add(modelNames[i]);
                }
            }
            if (modelNames.Contains(selected))
            {
                cbModelName.SelectedItem = selected;
            }
            else
            {
                cbModelName.SelectedItem = 0;
            }
        }
        private void dgvPanelResult_PreviewMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var dgv =  sender as DataGrid;
            int id = dgv.SelectedIndex;
            if (id > -1 && id < mPanelResults.Count)
            {
                Results.PanelResult panel = mPanelResults[id];
                Windows.ShowPanelWindow panelWindow = new Windows.ShowPanelWindow(panel);
                panelWindow.ShowDialog();
            }
        }
        private void UpdateLabelStatus()
        {
            int countGood = 0;
            int countPass = 0;
            int countNG = 0;
            for (int i = 0; i < mPanelResults.Count; i++)
            {
                if (mPanelResults[i].OPResult == "Good")
                    countGood++;
                if (mPanelResults[i].OPResult == "Pass")
                    countPass++;
                if (mPanelResults[i].OPResult == "NG")
                    countNG++;
            }
            int countTotal = countGood + countPass + countNG;
            double divTotal = countTotal == 0 ? 1 : countTotal;
            double yieldRate = (countGood + countPass) / divTotal;
            double ntf = (countPass) / divTotal;
            this.Dispatcher.Invoke(() => {
                lbTotal.Content = countTotal;
                lbPass.Content = countPass;
                lbNG.Content = countNG;
                lbGood.Content = countGood;
                lnYieldRate.Content = Math.Round(yieldRate * 100, 1).ToString() + "%";
                lbNTF.Content = Math.Round(ntf * 100, 1).ToString() + "%";
            });
        }
        private void btSearch_Click(object sender, RoutedEventArgs e)
        {
            string model = string.Empty;
            string sn = string.Empty;
            DateTime start = new DateTime(2021,01,01);
            DateTime end = DateTime.Now;
            bool good = false;
            bool pass = false;
            bool ng = false;

            if(cbModelName.SelectedIndex > 0)
            {
                model = cbModelName.SelectedItem.ToString();
            }
            sn = txtSN.Text;
            if(cStart.IsChecked == true)
            {
                start = dtStart.SelectedDate.GetValueOrDefault();
            }
            if (cEnd.IsChecked == true)
            {
                end = dtEnd.SelectedDate.GetValueOrDefault();
            }
            if(model.ToUpper() != "ALL" && model != string.Empty)
            {
                var m = Model.LoadModelByName(model);
                model = m.ModelName;
            }
            good = cGood.IsChecked == true;
            pass = cPass.IsChecked == true;
            ng = cNG.IsChecked == true;
            var results =  mMongo.Query(model, sn, start, end, good, pass, ng);
            mPanelResults.Clear();
            mPanelResults.AddRange(results);
            dgvPanelResult.ItemsSource = mPanelResults;
            dgvPanelResult.Items.Refresh();
            UpdateLabelStatus();
        }

        private void btChart_Click(object sender, RoutedEventArgs e)
        {
            int countGood = 0;
            int countPass = 0;
            int countNG = 0;
            for (int i = 0; i < mPanelResults.Count; i++)
            {
                if (mPanelResults[i].OPResult == "Good")
                    countGood++;
                if (mPanelResults[i].OPResult == "Pass")
                    countPass++;
                if (mPanelResults[i].OPResult == "NG")
                    countNG++;
            }
            Forms.VisualChartForm form = new Forms.VisualChartForm(countGood, countPass, countNG);
            form.ShowDialog();
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            Windows.SettingWindow window = new Windows.SettingWindow();
            window.ShowDialog();
        }
    }
}
