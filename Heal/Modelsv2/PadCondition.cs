﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;

namespace SPI_AOI.Modelsv2
{
    public class PadCondition
    {
        public string ConditionName { get; set; }
        public bool IsCheckMissing { get; set; }
        public bool IsCheckStain { get; set; }
        public bool IsCheckShapeFail { get; set; }
        public bool IsCheckOverArea { get; set; }
        public bool IsCheckLowArea { get; set; }
        public bool IsCheckOutVersion { get; set; }
        public bool IsCheckWeakTesion { get; set; }
        public Condition AreaLimit { get; set; }
        public Condition ShiftXLimit { get; set; }
        public Condition ShiftYLimit { get; set; }
        public PadCondition()
        {
            this.ConditionName = "DefaultCondition";
            this.IsCheckMissing = true;
            this.IsCheckStain = true;
            this.IsCheckShapeFail = true;
            this.IsCheckOutVersion = true;
            this.IsCheckWeakTesion = true;
            this.IsCheckOverArea = true;
            this.IsCheckLowArea = true;   
            this.AreaLimit = new Condition(500, 15000, 60, 260);
            this.ShiftXLimit = new Condition(0, 370, 0, 50);
            this.ShiftYLimit = new Condition(0, 370, 0, 50);
        }
        public PadCondition Copy()
        {
            PadCondition cond = new PadCondition();
            cond.ConditionName = this.ConditionName;
            cond.IsCheckMissing = this.IsCheckMissing;
            cond.IsCheckStain = this.IsCheckStain;
            cond.IsCheckShapeFail = this.IsCheckShapeFail;
            cond.IsCheckOutVersion = this.IsCheckOutVersion;
            cond.IsCheckWeakTesion = this.IsCheckWeakTesion;
            cond.IsCheckOverArea = this.IsCheckOverArea;
            cond.IsCheckLowArea = this.IsCheckLowArea;
            cond.AreaLimit = this.AreaLimit.Copy();
            cond.ShiftXLimit = this.ShiftXLimit.Copy();
            cond.ShiftYLimit = this.ShiftYLimit.Copy();
            return cond;
        }
        public static List<PadCondition> LoadCondition()
        {
            List<PadCondition> condition = new List<PadCondition>();
            Define define = new Define();
            string[] files = Directory.GetFiles(define.ConditionPath, "*.json");
            if(files != null)
            {
                for (int i = 0; i < files.Length; i++)
                {
                    string s = File.ReadAllText(files[i]);
                    try
                    {
                        PadCondition cond = JsonConvert.DeserializeObject<PadCondition>(s);
                        condition.Add(cond);
                    }
                    catch { continue; }
                }
            }
            return condition;
        }
        public void SaveCondition(string Name = null)
        {
            Define define = new Define();
            Name = Name == null ? this.ConditionName : Name;
            string s = JsonConvert.SerializeObject(this);
            string path = $"{define.ConditionPath}/{Name}.json";
            if (File.Exists(path))
                File.Delete(path);
            File.WriteAllText(path, s);
        }
        public static void DeleteCondition(string Name)
        {
            Define define = new Define();
            string[] files = Directory.GetFiles(define.ConditionPath, "*.json");
            if (files != null)
            {
                for (int i = 0; i < files.Length; i++)
                {
                    string s = File.ReadAllText(files[i]);
                    try
                    {
                        PadCondition cond = JsonConvert.DeserializeObject<PadCondition>(s);
                        if (cond.ConditionName == Name)
                            File.Delete(files[i]);
                    }
                    catch { continue; }
                }
            }
        }
        public static void CopyCondition(string path)
        {
            Define define = new Define();
            string s = File.ReadAllText(path);
            var listCond = LoadCondition();
            PadCondition cond = null;
            try
            {
                cond = JsonConvert.DeserializeObject<PadCondition>(s);
                
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message, "Error", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
                return;
            }
            for (int i = 0; i < listCond.Count; i++)
            {
                if(cond.ConditionName == listCond[i].ConditionName)
                {
                    System.Windows.MessageBox.Show("Condition is existed!!", "Error", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
                    return;
                }
            }
            File.WriteAllText($"{define.ConditionPath}/{cond.ConditionName}.json", s);
        }
        public class Condition
        {
            public bool Percent { get; set; }
            public double LowConst { get; set; }
            public double HighConst { get; set; }
            public double LowPercent { get; set; }
            public double HighPercent { get; set; }
            public Condition(double LowConst, double HighConst, double LowPercent, double HighPercent)
            {
                this.LowConst = LowConst;
                this.LowPercent = LowPercent;
                this.HighConst = HighConst;
                this.HighPercent = HighPercent;
                this.Percent = true;
            }
            public Condition Copy()
            {
                Condition cond = new Condition(this.LowConst, this.HighConst, this.LowPercent, this.HighPercent);
                cond.Percent = this.Percent;
                return cond;
            }
        }
    }
    
}
