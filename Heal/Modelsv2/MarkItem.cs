﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPI_AOI.Modelsv2
{
    public class MarkItem
    {
        public int Mark_ID { get; set; }
        public int Pad_ID { get; set; }

        public MarkItem(int ID)
        {
            this.Mark_ID = ID;
            this.Pad_ID = -1;
        }
    }
}
