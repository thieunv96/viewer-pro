﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace SPI_AOI.Modelsv2
{
    class Define
    {
        public string ModelPath = Heal.Properties.Settings.Default.APP_CONFIG_PATH + "\\Models";
        public string ConditionPath = Heal.Properties.Settings.Default.APP_CONFIG_PATH + "\\Conditions";
        public float DPI_Display = 450;
        public float DPI_Std = 800;
        public int ExtImage = 500;
        public Define()
        {
            if(!Directory.Exists(ModelPath))
            {
                Directory.CreateDirectory(ModelPath);
            }
            if (!Directory.Exists(ConditionPath))
            {
                Directory.CreateDirectory(ConditionPath);
            }
        }
    }
}
