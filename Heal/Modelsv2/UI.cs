﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emgu.CV.Structure;
using Emgu.CV;
using Emgu.CV.Util;
using System.Diagnostics;
using System.Drawing;
using Emgu.CV.CvEnum;


namespace SPI_AOI.Modelsv2
{
    public class UI
    {
        public static Image<Bgr, byte> GetImageFromModel(Model model)
        {
            Define define = new Define();
            double scale = define.DPI_Display / define.DPI_Std;
            if (model == null)
                return null;
            var gerber = model.Gerber;
            if (gerber.ImageHeight < 1 || gerber.ImageWidth < 1 || gerber.ImageHeight * (double)gerber.ImageWidth * 3 >= Math.Pow(2,31) )
                return null;
            Image<Bgr, byte> image = new Image<Bgr, byte>(Convert.ToInt32( gerber.ImageWidth* scale), Convert.ToInt32(gerber.ImageHeight * scale));
            
            var userSt = model.User;
            if (model.Gerber.Visible)
            {
                
                List<int> idSelected = new List<int>();
                for (int i = 0; i < model.User.ListItemSelected.Count; i++)
                {
                    var item = model.User.ListItemSelected[i];
                    if (item.Type == ItemType.Gerber)
                        idSelected.Add(item.Item_ID);
                }
                var colorPadNormal = new MCvScalar(0, 0, 255);
                var colorPadLinked = userSt.HightLightLinkedPad ?  new MCvScalar(255, 0, 0) : new MCvScalar(0, 0, 255);
                var colorPadSelected = new MCvScalar(255,255,255);
                var colorPadMark = new MCvScalar(0, 255, 255);
                using (VectorOfVectorOfPoint contoursNormal = new VectorOfVectorOfPoint())
                using (VectorOfVectorOfPoint contoursLinked = new VectorOfVectorOfPoint())
                using (VectorOfVectorOfPoint contoursMark = new VectorOfVectorOfPoint())
                using (VectorOfVectorOfPoint contoursSelect = new VectorOfVectorOfPoint())
                {
                    foreach (var item in gerber.PadItems)
                    {
                        if(item.Visible && (item.PCB_ID < 0 || (item.PCB_ID >= 0)))
                        {
                            if(gerber.PCBItems.Count == 0 && item.PCB_ID == 0)
                            {
                                if (idSelected.Contains(item.Pad_ID))
                                    contoursSelect.Push(new VectorOfPoint(item.ContourDisplay));
                                else if (item.CadItem_ID != -1 && item.CadFile_ID >= 0)
                                    contoursLinked.Push(new VectorOfPoint(item.ContourDisplay));
                                else
                                    contoursNormal.Push(new VectorOfPoint(item.ContourDisplay));
                            }
                            else if(gerber.PCBItems.Count > item.PCB_ID && item.PCB_ID >= 0)
                            {
                                if(gerber.PCBItems[item.PCB_ID].Visible)
                                {
                                    if (idSelected.Contains(item.Pad_ID))
                                        contoursSelect.Push(new VectorOfPoint(item.ContourDisplay));
                                    else if (item.CadItem_ID != -1 && item.CadFile_ID >= 0)
                                        contoursLinked.Push(new VectorOfPoint(item.ContourDisplay));
                                    else
                                        contoursNormal.Push(new VectorOfPoint(item.ContourDisplay));
                                }
                            }
                            else if(item.PCB_ID < 0)
                            {
                                if (idSelected.Contains(item.Pad_ID))
                                    contoursSelect.Push(new VectorOfPoint(item.ContourDisplay));
                                else if (item.CadItem_ID != -1 && item.CadFile_ID >= 0)
                                    contoursLinked.Push(new VectorOfPoint(item.ContourDisplay));
                                else
                                    contoursNormal.Push(new VectorOfPoint(item.ContourDisplay));
                            }
                        }
                    }
                    CvInvoke.DrawContours(image, contoursLinked, -1, colorPadLinked, -1);
                    CvInvoke.DrawContours(image, contoursNormal, -1, colorPadNormal, -1);
                    CvInvoke.DrawContours(image, contoursSelect, -1, colorPadSelected, -1);
                    CvInvoke.DrawContours(image, contoursMark, -1, colorPadMark, -1);
                }
                
                for (int i = 0; i < gerber.PCBItems.Count; i++)
                {
                    int thinkness = 3;
                    Rectangle bound = gerber.PCBItems[i].Bounding;
                    Rectangle rect = new Rectangle(bound.X, bound.Y, bound.Width, bound.Height);
                    rect.X = Convert.ToInt32(rect.X * scale) - thinkness;
                    rect.Y = Convert.ToInt32(rect.Y * scale) - thinkness;
                    rect.Width = Convert.ToInt32(rect.Width * scale) + 2 * thinkness;
                    rect.Height = Convert.ToInt32(rect.Height * scale) + 2 * thinkness;
                    if (userSt.ShowPCBBoudingRect)
                    {
                        CvInvoke.Rectangle(image, rect, new MCvScalar(0, 255, 255), thinkness);
                    }
                    if (userSt.ShowPCBName)
                    {
                        CvInvoke.PutText(image, gerber.PCBItems[i].PCBName, new Point(rect.X, rect.Y - 5),
                                 FontFace.HersheyDuplex, 1, new MCvScalar(0, 255, 255), 1);
                    }
                }
                
            }
            
            for (int i = 1; i < model.Cad.Count; i++)
            {
                CadFile cadFile = model.Cad[i];
                if (cadFile.Visible)
                {
                    List<int> ctSelected = new List<int>();
                    foreach (var item in model.User.ListItemSelected)
                    {
                        if(item.Type == ItemType.CAD && item.Perent_ID == cadFile.CadFile_ID)
                        {
                            ctSelected.Add(item.Item_ID);
                        }
                    }
                    Rectangle boundImage = new Rectangle(0, 0, image.Width, image.Height);
                    foreach (var item in cadFile.CadItems)
                    {
                        if (!boundImage.Contains(item.CenterDisplay))
                            continue;
                        var color = new MCvScalar(cadFile.Color.B, cadFile.Color.G, cadFile.Color.R);
                        if (userSt.ShowLinkLine && gerber.Visible)
                        {
                            for (int m = 0; m < item.PadAttached.Count; m++)
                            {
                                int padId = item.PadAttached[m];
                                if(padId < model.Gerber.PadItems.Count)
                                {
                                    PadItem pad = model.Gerber.PadItems[padId];
                                    if (pad.Visible && pad.CadFile_ID == item.CadFile_ID && pad.CadItem_ID == item.CadItem_ID)
                                    {
                                        if(pad.PCB_ID < 0 || (gerber.PCBItems[pad.PCB_ID].Visible && pad.PCB_ID >= 0))
                                        {
                                            Point ctPadStd = model.Gerber.PadItems[padId].Center;
                                            Point ctDisplay = new Point(Convert.ToInt32(ctPadStd.X * scale), Convert.ToInt32(ctPadStd.Y * scale));
                                            CvInvoke.Line(image, ctDisplay, item.CenterDisplay, color, 1);
                                        }
                                    }
                                }
                            }
                        }
                        if (userSt.ShowComponentName)
                        {
                            if (ctSelected.Contains(item.CadItem_ID)) 
                                CvInvoke.PutText(image, item.ComponentName, new Point(item.CenterDisplay.X - 10, item.CenterDisplay.Y - 5),
                                 FontFace.HersheyDuplex, 0.4, new MCvScalar(255,255,255), 1);
                            else
                                CvInvoke.PutText(image, item.ComponentName, new Point(item.CenterDisplay.X-10, item.CenterDisplay.Y - 5),
                                FontFace.HersheyDuplex, 0.4, color, 1);
                        }
                        if (userSt.ShowComponentCenter)
                        {
                            if(ctSelected.Contains(item.CadItem_ID))
                                CvInvoke.Circle(image, item.CenterDisplay, 2, new MCvScalar(255,255,255), -1);
                            else
                                CvInvoke.Circle(image, item.CenterDisplay, 2, color, -1);
                        }
                        
                    }
                }
            }
            return image;
        }

        public static Image<Bgr, byte> GetConditionImageFromModel(Model model, int IDPadSelected, List<int> ListIDPadSelected)
        {
            Define define = new Define();
            double scale = define.DPI_Display / define.DPI_Std;
            if (model == null)
                return null;
            var gerber = model.Gerber;
            if (gerber.ImageHeight < 1 || gerber.ImageWidth < 1 || gerber.ImageHeight * (double)gerber.ImageWidth * 3 >= Math.Pow(2, 31))
                return null;
            Image<Bgr, byte> image = new Image<Bgr, byte>(Convert.ToInt32(gerber.ImageWidth * scale), Convert.ToInt32(gerber.ImageHeight * scale), new Bgr(0,50,0));
            CvInvoke.Rectangle(image, new Rectangle(2, 2, image.Width - 5, image.Height - 5), new MCvScalar(50, 255, 255), 3);
            if (model.Gerber.Visible)
            {
                var colorPadNormal = new MCvScalar(255, 255, 255);
                var colorPadSelected = new MCvScalar(50, 50, 250);
                var colorListPadSelected = new MCvScalar(65, 125, 224);
                var colorPadMark = new MCvScalar(0, 255, 255);
                var colorUncheck = new MCvScalar(50, 50, 50);
                using (VectorOfVectorOfPoint contoursNormal = new VectorOfVectorOfPoint())
                using (VectorOfVectorOfPoint contoursPadSelected = new VectorOfVectorOfPoint())
                using (VectorOfVectorOfPoint contoursListPadSelected = new VectorOfVectorOfPoint())
                using (VectorOfVectorOfPoint contoursMark = new VectorOfVectorOfPoint())
                using (VectorOfVectorOfPoint contoursUncheck = new VectorOfVectorOfPoint())
                {
                    foreach (var item in gerber.PadItems)
                    {
                        for (int i = 0; i < model.Gerber.MarkItems.Count; i++)
                        {
                            var mark = model.Gerber.MarkItems[i];
                            if (mark.Pad_ID == item.Pad_ID)
                            {
                                contoursMark.Push(new VectorOfPoint(item.ContourDisplay));
                                CvInvoke.PutText(image, "G" + (i + 1).ToString(), new Point(Convert.ToInt32(item.Bouding.X * scale), Convert.ToInt32(item.Bouding.Y * scale) - 5),
                                    FontFace.HersheyDuplex, 0.4, colorPadMark, 1);
                                continue;
                            }
                        }
                        if (IDPadSelected == item.Pad_ID)
                        {
                            contoursPadSelected.Push(new VectorOfPoint(item.ContourDisplay));
                            continue;
                        }
                        if (ListIDPadSelected.Contains(item.Pad_ID))
                        {
                            contoursListPadSelected.Push(new VectorOfPoint(item.ContourDisplay));
                            continue;
                        }
                        else
                            contoursNormal.Push(new VectorOfPoint(item.ContourDisplay));
                        if(!item.Check)
                        {
                            contoursUncheck.Push(new VectorOfPoint(item.ContourDisplay));
                        }
                    }
                    CvInvoke.DrawContours(image, contoursNormal,            -1, colorPadNormal,         1);
                    CvInvoke.DrawContours(image, contoursUncheck, -1, colorUncheck, 1);
                    CvInvoke.DrawContours(image, contoursListPadSelected,   -1, colorListPadSelected,   1);
                    CvInvoke.DrawContours(image, contoursMark,              -1, colorPadMark,           1);
                    CvInvoke.DrawContours(image, contoursPadSelected,       -1, colorPadSelected,       1);
                    
                }

            }
            return image;
        }
        public static Image<Bgr, byte> GetFOVImageFromModel(Model model)
        {
            Define define = new Define();
            double scale = define.DPI_Display / define.DPI_Std;
            if (model == null)
                return null;
            var gerber = model.Gerber;
            if (gerber.ImageHeight < 1 || gerber.ImageWidth < 1 || gerber.ImageHeight * (double)gerber.ImageWidth * 3 >= Math.Pow(2, 31))
                return null;
            Image<Bgr, byte> image = new Image<Bgr, byte>(Convert.ToInt32(gerber.ImageWidth * scale), Convert.ToInt32(gerber.ImageHeight * scale), new Bgr(0, 50, 0));
            CvInvoke.Rectangle(image, new Rectangle(2, 2, image.Width - 5, image.Height - 5), new MCvScalar(50, 255, 255), 3);
            if (model.Gerber.Visible)
            {
                var colorPadNormal = new MCvScalar(255, 255, 255);
                using (VectorOfVectorOfPoint contoursNormal = new VectorOfVectorOfPoint())
                {
                    foreach (var item in gerber.PadItems)
                    {
                        contoursNormal.Push(new VectorOfPoint(item.ContourDisplay));
                    }
                    CvInvoke.DrawContours(image, contoursNormal, -1, colorPadNormal, 1);
                }
                Random rd = new Random();
                for (int i = 0; i < model.Gerber.FOVItems.Count; i++)
                {
                    FovItem fov = model.Gerber.FOVItems[i];
                    Point disCt = new Point(Convert.ToInt32(fov.Anchor.X * scale), Convert.ToInt32(fov.Anchor.Y * scale));
                    Rectangle disRect = new Rectangle(Convert.ToInt32(fov.ROIGerber.X * scale), Convert.ToInt32(fov.ROIGerber.Y * scale), Convert.ToInt32(fov.ROIGerber.Width * scale), Convert.ToInt32(fov.ROIGerber.Height * scale));
                    MCvScalar mCvScalar = new MCvScalar(rd.Next(0, 255), rd.Next(0, 255), rd.Next(0, 255));
                    CvInvoke.Rectangle(image, disRect, mCvScalar, 2);    
                    if(i > 0)
                    {
                        FovItem preFov = model.Gerber.FOVItems[i-1];
                        Point preDisCt = new Point(Convert.ToInt32(preFov.Anchor.X * scale), Convert.ToInt32(preFov.Anchor.Y * scale));
                        
                        CvInvoke.Line(image, preDisCt, disCt, new MCvScalar(255, 255, 255), 3);
                    }
                    disCt.Y -= 20;
                    CvInvoke.PutText(image, (i + 1).ToString(), disCt, FontFace.HersheyDuplex, 1, new MCvScalar(255,0,0), 2);
                }
            }
            return image;
        }
        public static Image<Bgr, byte> GetGerberImage(Model model)
        {
            if (model == null)
                return null;
            var gerber = model.Gerber;
            if (gerber.ImageHeight < 1 || gerber.ImageWidth < 1 || gerber.ImageHeight * (double)gerber.ImageWidth * 3 >= Math.Pow(2, 30))
                return null;
            Image<Bgr, byte> image = new Image<Bgr, byte>(Convert.ToInt32(gerber.ImageWidth), Convert.ToInt32(gerber.ImageHeight));
            using (VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint())
            {
                foreach (var item in gerber.PadItems)
                {
                    contours.Push(new VectorOfPoint(item.ContourStd));
                }
                CvInvoke.DrawContours(image, contours, -1, new MCvScalar(255, 255, 255), -1);
            }

            return image;
        }
    }
    public enum ViewMode
    {
        Render,
        Crop,
        Init,
        None,
        SelectPad
    }
}
