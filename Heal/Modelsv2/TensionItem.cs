﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace SPI_AOI.Modelsv2
{
    public class TensionItem
    {
        public int Tension_ID { get; set; }
        public bool Check { get; set; }
        public double ThreshValue { get; set; }
        public Point TensionPointStd { get; set; }
        public Point TensionPointReal { get; set; }
        public static List<TensionItem> GetTensionPoint(int Width, int Height, int NumTension, double ThreshValue)
        {
            Define define = new Define();
            int ext = define.ExtImage;
            int width = Width - 2 * define.ExtImage;
            int height = Height - 2 * define.ExtImage;
            List<TensionItem> tensionItems = new List<TensionItem>();
            for (int i = 0; i < NumTension; i++)
            {
                TensionItem tensionItem = new TensionItem();
                tensionItem.Tension_ID = i;
                tensionItem.Check = true;
                tensionItem.ThreshValue = ThreshValue;
                int idx = i % 3;
                int idy = i / 3;
                int x = width * (idx + 1) / 4 + ext;
                int y = height * (idy + 1) / 4 + ext;
                tensionItem.TensionPointStd = new Point(x, y);
                tensionItems.Add(tensionItem);
            }
            return tensionItems;
        }
    }
}
