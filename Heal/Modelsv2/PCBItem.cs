﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace SPI_AOI.Modelsv2
{
    public class PCBItem
    {
        public int PCB_ID { get; set; }
        public Rectangle Bounding { get; set; }
        public bool Check { get; set; }
        public bool Visible { get; set; }
        public string PCBName { get; set; }
        public List<int> PadItems { get; set; }
        public int IDFromIT { get; set; }
        public PCBItem(int PCB_ID)
        {
            this.PCB_ID = PCB_ID;
            this.PCBName = "PCB-" + PCB_ID.ToString();
            this.Bounding = Rectangle.Empty;
            this.Check = true;
            this.Visible = true;
            this.PadItems = new List<int>();
        }
        public void SetPCBName(int PCB_ID)
        {
            this.PCBName = "PCB-" + PCB_ID.ToString();
        }
        public PCBItem()
        {
            this.PCB_ID = -1;
            this.Bounding = Rectangle.Empty;
            this.Check = true;
            this.Visible = true;
            this.PadItems = new List<int>();
        }
    }
}
