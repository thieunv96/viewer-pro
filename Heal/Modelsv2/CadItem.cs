﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace SPI_AOI.Modelsv2
{
    public class CadItem
    {
        public int CadFile_ID { get; set; }
        public int CadItem_ID { get; set; }
        public string ComponentName { get; set; }
        public double Angle { get; set; }
        public string ComponentCode { get; set; }
        public List<int> PadAttached { get; set; }
        public Point CenterStd { get; set; }
        public Point CenterDisplay { get; set; }
        public CadItem() { }
        public CadItem Copy()
        {
            CadItem newCadItem = new CadItem();
            newCadItem.CadFile_ID = CadFile_ID;
            newCadItem.CadItem_ID = CadItem_ID;
            newCadItem.ComponentName = ComponentName;
            newCadItem.Angle = Angle;
            newCadItem.ComponentCode = ComponentCode;
            newCadItem.PadAttached = new List<int>(this.PadAttached.ToArray());
            newCadItem.CenterStd = new Point(this.CenterStd.X, this.CenterStd.Y);
            newCadItem.CenterDisplay = new Point(this.CenterDisplay.X, this.CenterDisplay.Y);
            return newCadItem;
        }
    }
}
