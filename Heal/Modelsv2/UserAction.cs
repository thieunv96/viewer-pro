﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Emgu.CV;
using Emgu.CV.Structure;

namespace SPI_AOI.Modelsv2
{
    public class UserAction
    {
        public bool ShowLinkLine { get; set; }
        public bool ShowComponentCenter { get; set; }
        public bool ShowComponentName { get; set; }
        public bool HightLightLinkedPad { get; set; }
        public bool ShowPCBBoudingRect { get; set; }
        public bool ShowPCBName { get; set; }
        public List<ItemSelected> ListItemSelected { get; set; }
        public ItemSelected ItemSelected { get; set; }
        public UserAction()
        {
            this.ShowLinkLine = true;
            this.ShowComponentCenter = true;
            this.ShowComponentName = true;
            this.HightLightLinkedPad = true;
            this.ShowPCBBoudingRect = true;
            this.ShowPCBName = true;
            this.ListItemSelected = new List<ItemSelected>();
            this.ItemSelected = null;
        }
        public void UnSelectItems(ItemType itemType)
        {
            int i = 0;
            int limit = this.ListItemSelected.Count;
            for (; i < limit;)
            {
                var item = this.ListItemSelected[i];
                if (item.Type == itemType || itemType == ItemType.ALL)
                {
                    this.ListItemSelected.Remove(item);
                    limit = this.ListItemSelected.Count;
                }
                else
                {
                    i++;
                }
            }
        }
        public void SelectItems(List<ItemSelected> items)
        {
            this.ListItemSelected.AddRange(items);
        }
    }
}
