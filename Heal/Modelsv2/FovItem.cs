﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace SPI_AOI.Modelsv2
{
    public class FovItem
    {
        public int ID { get; set; }
        public Point Anchor { get; set; }
        public Point RealAnchor { get; set; }
        public Rectangle ROIGerber { get; set; }
        public Rectangle RealROIGerber { get; set; }
        public Rectangle ROIImage { get; set; }
        public bool Check { get; set; }
        public List<int> PadItems { get; set; }
        public FovItem()
        {
            this.ROIGerber = new Rectangle();
            this.ROIImage = new Rectangle();
            this.Anchor = new Point(-1, -1);
            this.RealAnchor = new Point();
            this.PadItems = new List<int>();
        }
    }
}
