﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPI_AOI.Modelsv2
{
    public class GroupItem
    {
        public string GroupName { get; set; }
        public List<int> ListPadID { get; set; }
        public GroupItem(string GroupName)
        {
            this.GroupName = GroupName;
            this.ListPadID = new List<int>();
        }
    }
}
