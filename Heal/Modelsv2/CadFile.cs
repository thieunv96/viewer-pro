﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using NLog;
using Heal;
using System.IO;

namespace SPI_AOI.Modelsv2
{
    public class CadFile : ImportFile
    {
        public bool LoadSuccess { get; set; }
        public int CadFile_ID { get; set; }
        public bool Visible { get; set; }
        public Point CenterRotation { get; set; }
        public List<CadItem> CadItems { get; set; }
        #region "Load new Cad File"
        public CadFile(int CadFile_ID , string Path = null)
        {
            if(Path != null)
            {
                Define define = new Define();
                Random random = new Random();
                FileInfo fi = new FileInfo(Path);
                double scale = define.DPI_Display / define.DPI_Std;
                this.LoadSuccess = true;
                this.CadFile_ID = CadFile_ID;
                this.Color = Color.FromArgb(random.Next(256), random.Next(256), random.Next(256));
                this.FileName = fi.Name;
                this.FilePath = fi.FullName;
                this.Visible = true;
                this.CadItems = new List<CadItem>();
                string[] content = File.ReadAllLines(fi.FullName);
                double _XMin = 999999;
                double _YMin = 999999;
                double _XMax = -999999;
                double _YMax = -999999;
                int countID = 0;
                foreach (string item in content)
                {
                    string line = item.Replace('\t', ' ');
                    string[] arrall = line.Split(' ');
                    List<string> arr = new List<string>();
                    foreach (var str in arrall)
                    {
                        if (str != "")
                        {
                            arr.Add(str);
                        }
                    }
                    CadItem cadItem = new CadItem();
                    try
                    {
                        cadItem.CadFile_ID = this.CadFile_ID;
                        cadItem.ComponentName = arr[0];
                        cadItem.CenterStd = new Point(
                            Convert.ToInt32(Convert.ToDouble(arr[1]) * define.DPI_Std / 25.4),
                            Convert.ToInt32(Convert.ToDouble(arr[2]) * define.DPI_Std / 25.4));
                        cadItem.Angle = Convert.ToDouble(arr[3]);
                        cadItem.ComponentCode = arr[4];
                        cadItem.PadAttached = new List<int>();
                        cadItem.CadItem_ID = countID;
                        this.CadItems.Add(cadItem);
                        if (cadItem.CenterStd.X < _XMin)
                        {
                            _XMin = cadItem.CenterStd.X;
                        }
                        if (cadItem.CenterStd.Y < _YMin)
                        {
                            _YMin = cadItem.CenterStd.Y;
                        }
                        if (cadItem.CenterStd.X > _XMax)
                        {
                            _XMax = cadItem.CenterStd.X;
                        }
                        if (cadItem.CenterStd.Y > _YMax)
                        {
                            _YMax = cadItem.CenterStd.Y;
                        }
                    }
                    catch (Exception)
                    {
                        this.Dispose();
                        this.LoadSuccess = false;
                        this.CadItems.Clear();
                    }
                    countID++;
                }
                if(this.LoadSuccess)
                {
                    // calculate rotate point
                    // transfer to origin of coordinates
                    double x =  -_XMin;
                    double y =  -_YMin ;
                    for (int i = 0; i < this.CadItems.Count; i++)
                    {
                        Point ct = this.CadItems[i].CenterStd;
                        this.CadItems[i].CenterStd = new Point(Convert.ToInt32(ct.X + x), Convert.ToInt32(ct.Y + y));
                        this.CadItems[i].CenterDisplay = new Point(Convert.ToInt32(scale * this.CadItems[i].CenterStd.X), Convert.ToInt32(this.CadItems[i].CenterStd.Y * scale));
                    }
                    _XMin += x;
                    _XMax += x;
                    _YMax += y;
                    _YMin += y;
                    this.CenterRotation = new Point((int)(_XMax - _XMin) / 2, (int)(_YMax - _YMin) / 2);
                }
            }
        }
        public static CadFile GetCadUNDEFINE(int CadFile_ID)
        {
            CadFile cad = new CadFile(0);
            cad.LoadSuccess = true;
            cad.CadFile_ID = CadFile_ID;
            cad.Color = Color.FromArgb(255,0,150);
            cad.FileName = string.Empty;
            cad.FilePath = string.Empty;
            cad.Visible = false;
            cad.CadItems = new List<CadItem>();
            CadItem item = new CadItem();
            item.Angle = 0;
            item.CadFile_ID = CadFile_ID;
            item.CadFile_ID = 0;
            item.CenterStd = new Point(-1, -1);
            item.CenterDisplay = new Point(-1, -1);
            item.ComponentCode = "NO CODE";
            item.ComponentName = "NO NAME";
            item.PadAttached = new List<int>();
            cad.CadItems.Add(item);
            return cad;
        }
        #endregion
        #region "Transformation"
        public void Flip(bool X, bool Y, float ValueFlip = 0)
        {
            if (this.CadItems.Count > 0)
            {
                float min = 65535;
                float max = 0;
                for (int i = 0; i < this.CadItems.Count; i++)
                {
                    if(X)
                    {
                        min = min > this.CadItems[i].CenterStd.X ? this.CadItems[i].CenterStd.X : min;
                        max = max < this.CadItems[i].CenterStd.X ? this.CadItems[i].CenterStd.X : max;
                    }
                    else if(Y)
                    {
                        min = min > this.CadItems[i].CenterStd.Y ? this.CadItems[i].CenterStd.Y : min;
                        max = max < this.CadItems[i].CenterStd.Y ? this.CadItems[i].CenterStd.Y : max;
                    }
                }
                float valFlip = ValueFlip == 0 ? (min + max) / 2 : ValueFlip;
                for (int i = 0; i < this.CadItems.Count; i++)
                {
                    Point ct = this.CadItems[i].CenterStd;
                    if(X)
                        this.CadItems[i].CenterStd = new Point(Convert.ToInt32(ct.X + 2 * (valFlip - ct.X)), ct.Y);
                    else if(Y)
                        this.CadItems[i].CenterStd = new Point(ct.X, Convert.ToInt32(ct.Y + 2 * (valFlip - ct.Y)));
                }
            }
            UpdateDisplayPoint();
        }
        public void Shift(int X, int Y)
        {
            for (int i = 0; i < this.CadItems.Count; i++)
            {
                var item = this.CadItems[i].CenterStd;
                item.X += X;
                item.Y += Y;
                this.CadItems[i].CenterStd = item;
            }
            var ctRotate = this.CenterRotation;
            ctRotate.X += X;
            ctRotate.Y += Y;
            this.CenterRotation = ctRotate;
            UpdateDisplayPoint();
        }
        public void Rotation(double Angle, Point Center = new Point(), DataType dataType = DataType.Standard)
        {
            Define define = new Define();
            double scale = define.DPI_Display / define.DPI_Std;
            if(dataType == DataType.Display)
            {
                Center.X = Convert.ToInt32(Center.X / scale);
                Center.Y = Convert.ToInt32(Center.Y / scale);
            }
            Point ct = (Center == new Point()) ? this.CenterRotation : Center;
            for (int i = 0; i < this.CadItems.Count; i++)
            {
                this.CadItems[i].CenterStd = ModelUtils.PointRotate(this.CadItems[i].CenterStd, ct, Angle * Math.PI / 180.0);
            }
            UpdateDisplayPoint();
        }
        private void UpdateDisplayPoint()
        {
            Define define = new Define();
            double scale = define.DPI_Display / define.DPI_Std;
            for (int i = 0; i < this.CadItems.Count; i++)
            {
                this.CadItems[i].CenterDisplay = new Point(Convert.ToInt32(scale * this.CadItems[i].CenterStd.X), Convert.ToInt32(this.CadItems[i].CenterStd.Y * scale));
            }
        }
        public void SetCadID(int ID)
        {
            this.CadFile_ID = ID;
            foreach (var item in CadItems)
            {
                item.CadFile_ID = ID;
            }
        }
        public CadFile Copy(int X = 0, int Y = 0)
        {
            CadFile cadFile = new CadFile(0);

            return cadFile;
        }
        #endregion
        #region "Action"
        public List<ItemSelected> SelectItems(Rectangle Rect, DataType DataType = DataType.Display)
        {
            List<ItemSelected> listCenterSelected = new List<ItemSelected>();
            if (Rect == null || Rect == Rectangle.Empty)
                return listCenterSelected;
            Define define = new Define();
            double scale = define.DPI_Std / define.DPI_Display;
            Rectangle rectStd = Rect;
            if (DataType == DataType.Display)
                rectStd = new Rectangle(Convert.ToInt32(Rect.X * scale), Convert.ToInt32(Rect.Y * scale), Convert.ToInt32(Rect.Width * scale), Convert.ToInt32(Rect.Height * scale));
            foreach (var item in this.CadItems)
            {
                if (rectStd.Contains(item.CenterStd)) 
                {
                    ItemSelected slItem = new ItemSelected();
                    slItem.Type = ItemType.CAD;
                    slItem.Perent_ID = item.CadFile_ID;
                    slItem.Item_ID = item.CadItem_ID;
                    listCenterSelected.Add(slItem);
                }
            }
            return listCenterSelected;
        }
        public Point GetCenterOfCadSelected(List<int> listCenterID, DataType dataType = DataType.Standard)
        {
            Define define = new Define();
            double scale = define.DPI_Display / define.DPI_Std;
            Point ct = new Point(-1, -1);
            if (listCenterID.Count > 0)
            {
                long x = 0;
                long y = 0;
                for (int i = 0; i < this.CadItems.Count; i++)
                {
                    if (listCenterID.Contains(i))
                    {
                        x += this.CadItems[i].CenterStd.X;
                        y += this.CadItems[i].CenterStd.Y;
                    }
                }
                x = x / listCenterID.Count;
                y = y / listCenterID.Count;
                if (dataType == DataType.Standard)
                    ct = new Point(Convert.ToInt32(x), Convert.ToInt32(y));
                else
                    ct = new Point(Convert.ToInt32(x * scale), Convert.ToInt32(y * scale));
            }
            return ct;
        }
        public void UnlinkComponent()
        {
            foreach (var item in CadItems)
            {
                item.PadAttached.Clear();
            }
        }
        #endregion
        public void Dispose()
        {
            this.CadItems.Clear();
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }
        public CadFile Copy()
        {
            CadFile newCadFile = new CadFile(this.CadFile_ID);
            newCadFile.CenterRotation = new Point(this.CenterRotation.X, this.CenterRotation.Y);
            newCadFile.Color = Color.FromArgb(this.Color.R, this.Color.G, this.Color.B);
            newCadFile.FileName = this.FileName;
            newCadFile.FilePath = this.FilePath;
            newCadFile.LoadSuccess = this.LoadSuccess;
            newCadFile.Time = this.Time;
            newCadFile.Visible = this.Visible;
            newCadFile.CadItems = new List<CadItem>();
            for (int i = 0; i < this.CadItems.Count; i++)
            {
                newCadFile.CadItems.Add(this.CadItems[i].Copy());
            }
            return newCadFile;
        }
    }
}