﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Emgu.CV;
using Emgu.CV.Structure;

namespace SPI_AOI.Modelsv2
{
    public class ModelConfig
    {
        public float Real_DPI { get; set; }
        public Size FOV { get; set; }
        public Side ModelSide { get; set; }
        public string Version { get; set; }
        public bool AutoPassEmtyFOV { get; set; }
        public double PulPerPixelX { get; set; }
        public double PulPerPixelY { get; set; }
        public double AngleXCamera { get; set; }
        public double AngleXYAxis { get; set; }
        public double PCBThickness { get; set; }
        public ModelConfig()
        {
            
        }
    }
}
