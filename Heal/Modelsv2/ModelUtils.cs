﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;


namespace SPI_AOI.Modelsv2
{
    public class ImportFile
    {
        public string FilePath { get; set; }
        public string FileName { get; set; }
        public DateTime Time { get; set; }
        public Color Color { get; set; }
    }
    public class ItemSelected
    {
        public ItemType Type { get; set; }
        public int Perent_ID { get; set; }
        public int Item_ID { get; set; }
        public ItemSelected()
        {
            this.Perent_ID = -1;
            this.Item_ID = -1;
        }
    }
    public class ModelUtils
    {
        public static Point PointRotate(Point RotatePoint, Point Center, double Angle)
        {
            int x = RotatePoint.X - Center.X;
            int y = RotatePoint.Y - Center.Y;
            int x1 = (int)Math.Round(x * Math.Cos(Angle) - y * Math.Sin(Angle));
            int y1 = (int)Math.Round(x * Math.Sin(Angle) + y * Math.Cos(Angle));
            RotatePoint.X = x1 + Center.X;
            RotatePoint.Y = y1 + Center.Y;
            return RotatePoint;
        }
        public static double Distance(Point P1, Point P2)
        {
            return Math.Sqrt(Math.Pow(P2.X - P1.X, 2) + Math.Pow(P2.Y - P1.Y, 2));
        }
        public static double DistancePadnComponent(Point Component, Point Pad, double angleComponent)
        {
            System.Windows.Vector refVector = new System.Windows.Vector(0,1);
            System.Windows.Vector crVector = new System.Windows.Vector(Pad.X - Component.X, Pad.Y - Component.Y);
            if (angleComponent % 90 != 0)
                refVector = new System.Windows.Vector(1, 1);
            double angleVt = System.Windows.Vector.AngleBetween(refVector, crVector);
            angleVt = Math.Abs(angleVt % 90);
            double hypotenuse = Distance(Pad, Component);
            double distance = Math.Max(hypotenuse * Math.Cos(angleVt * Math.PI / 180.0), (hypotenuse * Math.Cos((90 - angleVt) * Math.PI / 180.0)));
            return distance;
        }
        
    }
    public enum ItemType
    {
        Gerber,
        CAD,
        ALL
    }
    public enum Side
    {
        TOP,
        BOT
    }
    public enum VI_Mode
    {
        DeepLearning,
        OpenCV
    }
    public enum PadInRectMode
    {
        All,
        Linked,
        NotLink
    }
    public enum DataType
    {
        Display,
        Standard
    }
    public enum LinkMode
    {
        RnC,
        TwoPad,
        All
    }
    public enum FindPadMode
    {
        TheSamePad,
        TheSameComponentName,
        TheSameComponentCode,
        None
    }
}
