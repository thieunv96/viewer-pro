﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPI_AOI.Modelsv2
{
    public class HardwareConfig
    {
        public Work VIByOpenCV { get; set; }
        public Work VIByDeep { get; set; }
        public Work Scan { get; set; }
        public Work Mark { get; set; }
        public int Conveyor { get; set; }
        public HardwareConfig()
        {
            this.VIByOpenCV = new Work();
            this.VIByDeep = new Work();
            this.Scan = new Work();
            this.Mark = new Work();
            this.Conveyor = 1;
        }
        public class CameraSt
        {
            public int ExposureTime { get; set; }
            public double Gamma { get; set; }
            public CameraSt()
            {
                this.ExposureTime = 5000;
                this.Gamma = 1.0;
            }
            public CameraSt Copy()
            {
                CameraSt st = new CameraSt();
                st.ExposureTime = this.ExposureTime;
                st.Gamma = this.Gamma;
                return st;
            }
            
        }
        public class LightSourceSt
        {
            public int CH1 { get; set; }
            public int CH2 { get; set; }
            public int CH3 { get; set; }
            public int CH4 { get; set; }
            public LightSourceSt()
            {
                this.CH1 = 255;
                this.CH2 = 75;
                this.CH3 = 0;
                this.CH4 = 40;
            }
            public LightSourceSt Copy()
            {
                LightSourceSt st = new LightSourceSt();
                st.CH1 = this.CH1;
                st.CH2 = this.CH2;
                st.CH3 = this.CH3;
                st.CH4 = this.CH4;
                return st;
            }
        }
        public class Work
        {
            public CameraSt Camera { get; set; }
            public LightSourceSt LightSource { get; set; }
            public double SearchWidth { get; set; }
            public double SearchHeight { get;  set; }
            public double MatchingScore { get; set; }
            public int GrayLevel { get; set; }
            public int X { get; set; }
            public int Y { get; set; }
            public Side Side { get; set; }
            public Work()
            {
                this.Camera = new CameraSt();
                this.LightSource = new LightSourceSt();
                this.SearchWidth = 8;
                this.SearchHeight = 8;
                this.MatchingScore = 70;
                this.GrayLevel = 127;
                this.Side = Side.TOP;
            }
            public Work Copy()
            {
                Work w = new Work();
                w.Camera = this.Camera.Copy();
                w.LightSource = this.LightSource.Copy();
                w.GrayLevel = this.GrayLevel;
                w.SearchHeight = this.SearchHeight;
                w.SearchWidth = this.SearchWidth;
                w.MatchingScore = this.MatchingScore;
                w.X = this.X;
                w.Y = this.Y;
                w.Side = this.Side;
                return w;
            }
        }
       
    }
}
