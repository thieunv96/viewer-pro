﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Emgu.CV;
using Emgu.CV.Structure;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Emgu.CV.Util;
using System.Diagnostics;
using System.Threading;
using System.Runtime;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace SPI_AOI.Modelsv2
{
    public class Model
    {
        public string Model_ID { get; set; }
        public string ModelName { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime ModifyTime { get; set; }
        public VI_Mode VIMode { get; set; }
        public bool IsScan { get; set; }
        public bool CanRun { get; set; }
        public GerberFile Gerber { get; set; }
        public List<CadFile> Cad { get; set; }
        public HardwareConfig Hardware { get; set; }
        public ModelConfig Config { get; set; }
        public UserAction User { get; set; }

        public Model()
        {

        }
        public Model(string ModelName)
        {
            this.Model_ID = Guid.NewGuid().ToString().ToUpper();
            this.ModelName = ModelName;
            this.CreateTime = DateTime.Now;
            this.ModifyTime = DateTime.Now;
            this.CanRun = false;
            this.VIMode = VI_Mode.DeepLearning;
            this.IsScan = false;
            this.Gerber = new GerberFile();
            this.Cad = new List<CadFile>();
            this.Cad.Add(CadFile.GetCadUNDEFINE(0));
            this.Hardware = new HardwareConfig();
            this.Config = new ModelConfig();
            this.User = new UserAction();
        }
        #region model action
        public static string[] GetModelNames()
        {
            Define define = new Define();
            string[] mListModelNames = Directory.GetFiles(define.ModelPath, "*.json");
            for (int i = 0; i < mListModelNames.Length; i++)
            {
                FileInfo fi = new FileInfo(mListModelNames[i]);
                mListModelNames[i] = fi.Name.Replace(".json", "");
            }
            return mListModelNames;
        }
        public string SaveModel(string modelPath = null)
        {
            
            Define define = new Define();
            if(modelPath == null)
                modelPath = $"{define.ModelPath}/{this.ModelName} ({this.Config.Version.ToString()} - {this.Config.ModelSide.ToString()}).json";
            try
            {
                string json = JsonConvert.SerializeObject(this);
                File.WriteAllText(modelPath, json);
            }
            catch
            {
                return null;
            }
            finally
            {
            }
            return new FileInfo(modelPath).FullName;
        }
        public bool Delete(string model_name = null)
        {
            Define define = new Define();
            if (model_name == null)
                model_name = this.ModelName;
            string modelPath = define.ModelPath + "/" + model_name + ".json";
            try
            {
                File.Delete(modelPath);
            }
            catch
            {
                return false;
            }
            this.Dispose();
            return true;
        }
        public string SaveAsModel(string ModelName)
        {
            
            Define define = new Define();
            string modelIDOld = this.Model_ID;
            this.Model_ID = Guid.NewGuid().ToString().ToUpper();
            string modelPath = $"{define.ModelPath}/{ModelName} ({this.Config.Version.ToString()} - {this.Config.ModelSide.ToString()}).json";
            if (File.Exists(modelPath))
                return null;
            try
            {
                
                string json = JsonConvert.SerializeObject(this);
               
                File.WriteAllText(modelPath, json);
            }
            catch
            {
                return null;
            }
            finally
            {
                this.Model_ID = modelIDOld;
            }
            return new FileInfo(modelPath).FullName;
        }
        public static int ImportModel(string FilePath)
        {
            string s = File.ReadAllText(FilePath);
            Model model = null;
            try
            {
                model = JsonConvert.DeserializeObject<Model>(s);
            }
            catch 
            {
                return -1;
            }
            string[] names = GetModelNames();
            string importName = $"{model.ModelName} ({model.Config.Version.ToString()} - {model.Config.ModelSide.ToString()})";
            if (names.Contains(importName))
                return -2;
            else
            {
                model.SaveModel();
                return 0;
            }
        }
        public int ExportModel(string FilePath)
        {
            string jsonStr = JsonConvert.SerializeObject(this);
            File.WriteAllText(FilePath, jsonStr);
            return 0;
        }
        public static Model LoadModelByPath(string Path)
        {
            Model model = null;
            try
            {
                string s = File.ReadAllText(Path);
                model = JsonConvert.DeserializeObject<Model>(s);
            }
            catch
            {
                return null;
            }
            if (model != null)
            {
                for (int i = 0; i < model.Cad.Count; i++)
                {
                    model.Cad[i].CadFile_ID = i;
                }
            }
            return model;
        }
        public static Model LoadModelByName(string ModelName)
        {
            string Path = GetPathModelByName(ModelName);
            return LoadModelByPath(Path);
        }
        private static string GetPathModelByName(string modelName)
        {
            Define define = new Define();
            string[] mListModelNames = Directory.GetFiles(define.ModelPath, "*.json");
            for (int i = 0; i < mListModelNames.Length; i++)
            {
                FileInfo fi = new FileInfo(mListModelNames[i]);
                string modelPath = fi.Name.Replace(".json", "");
                string namePath = modelPath.Split('(')[0].Trim();
                if (namePath == modelName || modelPath == modelName)
                {
                    return fi.FullName;
                }
            }
            return null;
        }
        public Image<Bgr, byte> Getdiagram()
        {
            Define define = new Define();
            double scale = define.DPI_Display / define.DPI_Std;
            Image<Bgr, byte> image = new Image<Bgr, byte>(Convert.ToInt32(this.Gerber.ImageWidth * scale), 
                Convert.ToInt32(this.Gerber.ImageHeight * scale), new Bgr(0, 40, 0));
            using (VectorOfVectorOfPoint cntNormal = new VectorOfVectorOfPoint())
            using (VectorOfVectorOfPoint cntUntest = new VectorOfVectorOfPoint())
            using (VectorOfVectorOfPoint cntMark = new VectorOfVectorOfPoint())
            {
                List<int> idMark = new List<int>();
                for (int i = 0; i < this.Gerber.MarkItems.Count; i++)
                {
                    idMark.Add(this.Gerber.MarkItems[i].Pad_ID);
                }
                for (int i = 0; i < this.Gerber.PadItems.Count; i++)
                {
                    var pad = this.Gerber.PadItems[i];
                    if(idMark.Contains(pad.Pad_ID))
                    {
                        cntMark.Push(new VectorOfPoint(pad.ContourDisplay));
                    }
                    else if(!pad.Check)
                    {
                        cntUntest.Push(new VectorOfPoint(pad.ContourDisplay));
                    }
                    else if (pad.Check)
                    {
                        cntNormal.Push(new VectorOfPoint(pad.ContourDisplay));
                    }
                }
                CvInvoke.DrawContours(image, cntNormal, -1, new MCvScalar(255, 255, 255), -1);
                CvInvoke.DrawContours(image, cntUntest, -1, new MCvScalar(50, 50, 50), -1);
                CvInvoke.DrawContours(image, cntMark, -1, new MCvScalar(0, 255, 255), -1);
            }
            Rectangle Bound = new Rectangle(10, 10, image.Width - 20, image.Height - 20);
            CvInvoke.Rectangle(image, Bound, new MCvScalar(120, 120, 120), 5);
            return image;
        }
        public List<System.Windows.Rect> GetFOVDiagram(Size FOV)
        {
            Define define = new Define();
            double scale = define.DPI_Display / define.DPI_Std;
            List<System.Windows.Rect> fovs = new List<System.Windows.Rect>();
            double w = FOV.Width * scale;
            double h = FOV.Height * scale;
            for (int i = 0; i < this.Gerber.FOVItems.Count; i++)
            {
                var fov = this.Gerber.FOVItems[i];
                System.Windows.Rect rect = new System.Windows.Rect(scale * fov.Anchor.X - w / 2, scale * fov.Anchor.Y - h/2, w, h);
                fovs.Add(rect);
            }
            return fovs;
        }
        public List<System.Windows.Rect> GetTensionDiagram(Size FOV)
        {
            Define define = new Define();
            double scale = define.DPI_Display / define.DPI_Std;
            List<System.Windows.Rect> fovs = new List<System.Windows.Rect>();
            double w = FOV.Width * scale;
            double h = FOV.Height * scale;
            for (int i = 0; i < this.Gerber.TensionItems.Count; i++)
            {
                var tension = this.Gerber.TensionItems[i];
                System.Windows.Rect rect = new System.Windows.Rect(scale * tension.TensionPointStd.X - w / 2, scale * tension.TensionPointStd.Y - h / 2, w, h);
                fovs.Add(rect);
            }
            return fovs;
        }
        public void Dispose()
        {
            this.Gerber.Clear();
            this.Cad.Clear();
            this.User.UnSelectItems(ItemType.ALL);
        }
        #endregion

        #region Gerber action
        public void LoadGerber(string FilePath)
        {
            this.Unlink();
            this.Gerber.LoadFile(FilePath);
        }
        public void Rotate(double angle)
        {
            this.Unlink();
            this.Gerber.Rotate(angle);

        }
        public void Crop(Rectangle Rect)
        {
            this.Unlink();
            this.Gerber.Crop(Rect);
        }
        public void RenderFOV(Size FOV, double DPI, Size ImageSize, Utils.START_POINT stPoint)
        {
            this.Gerber.RenderFOV(FOV, DPI, ImageSize, stPoint);
        }
        public void RemoveGerber()
        {
            this.Gerber.Clear();
            this.Unlink();
        }
        public bool IsCanRun()
        {
            if (this.Gerber == null) return false;
            if (this.Gerber.PadItems.Count < 2) return false;
            if (this.Gerber.MarkItems.Count < 2) return false;
            if (this.Gerber.FOVItems.Count <= 0) return false;
            if (this.Hardware.Mark.X == 0 && this.Hardware.Mark.Y == 0) return false;
            return true;
        }
        public void ConvertRealPad(double Real_DPI, Size FOV)
        {
            this.Gerber.ConvertRealPad(Real_DPI, FOV);
            this.SortPinCad();
        }
        public void RemovePad(List<int> ListPadID)
        {
            this.Gerber.RemovePad(ListPadID);
        }
        public List<ItemSelected> SelectPadInRect(Rectangle Rect, PadInRectMode Mode)
        {
            this.User.UnSelectItems(ItemType.Gerber);
            var padInRect =  this.Gerber.PadInRect(Rect, Mode);
            this.User.SelectItems(padInRect);
            return padInRect;
        }
        public void SelectCadInRect(Rectangle Rect, int CadId = -1)
        {
            this.User.UnSelectItems(ItemType.CAD);
            for (int i = 1; i < this.Cad.Count; i++)
            {
                if(CadId == -1 || CadId == i)
                {
                    var listCt = this.Cad[i].SelectItems(Rect);
                    if (listCt.Count > 0)
                    {
                        this.User.SelectItems(this.Cad[i].SelectItems(Rect));
                    }
                }
            }
        }
        public List<object> GetLayerInRect(Rectangle Rect, PadInRectMode Mode)
        {
            List<object> layer = new List<object>();
            var padInRect = this.Gerber.PadInRect(Rect, PadInRectMode.All);
            if (padInRect.Count > 0)
                layer.Add(this.Gerber);
            for (int i = 1; i < this.Cad.Count; i++)
            {
                var listCt = this.Cad[i].SelectItems(Rect);
                if(listCt.Count > 0)
                {
                    layer.Add(this.Cad[i]);
                }
            }
            return layer;
        }
        public void ClearSelectPad()
        {
            this.User.UnSelectItems(ItemType.Gerber);
        }
        public Point GetCenterOfPadSelected(List<int> listIDPad)
        {
            return this.Gerber.GetCenterOfPadSelected(listIDPad, DataType.Standard);
        }
        #endregion

        #region CAD action

        public bool LoadCad(string FilePath)
        {
            CadFile cad = new CadFile(this.Cad.Count, FilePath);
            if(cad.CadItems.Count > 0)
            {
                this.Cad.Add(cad);
                return true;
            }
            return false;
        }
        public void RemoveCad(CadFile cad)
        {
            this.Cad.Remove(cad);
            for (int i = 1; i < this.Cad.Count; i++)
            {
                var cadFile = this.Cad[i];
                cadFile.CadFile_ID = i;
                cadFile.SetCadID(i);
                foreach (var item in cadFile.CadItems)
                {
                    this.Gerber.ResetCadID(item.PadAttached, i);
                }
            }
        }
        private double ScoresDirectCad(Point Center, int Width, int Height, double Angle)
        {
            double scale = 100.0;
            double x = Center.X / scale;
            double y = Center.Y / scale;
            double w = Width / scale;
            double h = Height / scale;

            double dis = 0;
            double x1 = 0;
            double y1 = 0;
            double x2 = 0;
            double y2 = 0;
            switch (Angle)
            {
                case 0:
                    x1 = 0;
                    y1 = 0;
                    x2 = x;
                    y2 = y;
                    break;
                case 90:
                    x1 = w;
                    y1 = 0;
                    x2 = w - x;
                    y2 = y;
                    break;
                case 180:
                    x1 = w;
                    y1 = h;
                    x2 = w - x;
                    y2 = h - y;
                    break;
                case 270:
                    x1 = 0;
                    y1 = h;
                    x2 = x;
                    y2 = h - y;
                    break;
                default:
                    break;
            }
            dis = Math.Pow(x2 - x1, 2) + Math.Pow(y2 - y1, 2);
            return dis;
        }
        public void SortPinCad()
        {
            int w = this.Gerber.ImageWidth;
            int h = this.Gerber.ImageHeight;
            double angle = this.Gerber.GerberAngle;

            for (int i = 0; i < Cad.Count; i++)
            {
                for (int j = 0; j < Cad[i].CadItems.Count; j++)
                {
                    CadItem cadItem = Cad[i].CadItems[j];
                    List<PadItem> padInCad = new List<PadItem>();
                    foreach (var id in cadItem.PadAttached)
                    {
                        padInCad.Add(this.Gerber.PadItems[id]);
                    }
                    var sorted = padInCad.OrderBy(x => ScoresDirectCad(x.Center, w, h, angle));
                    for (int k = 0; k < padInCad.Count; k++)
                    {
                        sorted.ElementAt(k).ComponentPinID = k;
                    }
                }
            }
        }
        public void ClearSelectCad()
        {
            this.User.UnSelectItems(ItemType.CAD);
        }
        public Point GetCenterOfCadSelected(List<int> listIDCenter, int CadId)
        {
            if (CadId >= this.Cad.Count)
                return new Point(-1, -1);
            return this.Cad[CadId].GetCenterOfCadSelected(listIDCenter, DataType.Standard);
        }
        #endregion
        public void AutoLinkRnCComponent()
        {
            for (int i = 1; i < this.Cad.Count; i++)
            {
                for (int j = 0; j < this.Cad[i].CadItems.Count; j++)
                {
                    var cadItem = this.Cad[i].CadItems[j];
                    string name = Convert.ToString(cadItem.ComponentName[0]);
                    string nextName = Convert.ToString(cadItem.ComponentName[1]);
                    if ((name.ToUpper() == "R" || name.ToUpper() == "C") && "0123456789".Contains(nextName))
                    {
                        var sortPad = this.Gerber.PadItems.OrderBy(pad => ModelUtils.DistancePadnComponent(pad.Center, cadItem.CenterStd, cadItem.Angle));
                        int count = 0;
                        int _id = 0;
                        while (true)
                        {
                            int id = sortPad.ElementAt(_id).Pad_ID;
                            var pad = this.Gerber.PadItems[id];
                            if (pad.Visible)
                            {
                                this.Gerber.LinkPad(id, cadItem.CadFile_ID, cadItem.CadItem_ID);
                                cadItem.PadAttached.Add(id);
                                count++;
                            }
                            if (count == 2)
                                break;
                            _id++;
                        }
                    }
                }
            }
        }
        public void AutoLink2n3PadComponent()
        {
            List<PadItem> listPadUnlink = new List<PadItem>();
            for (int i = 0; i < this.Gerber.PadItems.Count; i++)
            {
                var pad = this.Gerber.PadItems[i];
                if (pad.CadFile_ID < 0 && pad.CadItem_ID < 0)
                    listPadUnlink.Add(pad);

            }
            for (int i = 1; i < this.Cad.Count; i++)
            {
                for (int j = 0; j < this.Cad[i].CadItems.Count; j++)
                {
                    var cadItem = this.Cad[i].CadItems[j];
                    string name = Convert.ToString(cadItem.ComponentName[0]);
                    string nextName = Convert.ToString(cadItem.ComponentName[1]);
                    if ((name.ToUpper() == "R" || name.ToUpper() == "C") && "0123456789".Contains(nextName))
                        continue;
                    if (!(cadItem.ComponentName.Contains("SC") ||
                        (name.ToUpper() == "U" && "0123456789".Contains(nextName))))
                    {
                        var sortPad = listPadUnlink.OrderBy(pad => ModelUtils.Distance(pad.Center, cadItem.CenterStd));
                        int lm = 4;
                        PadItem[] padsCheck = new PadItem[lm];
                        double[] distance = new double[lm];
                        double[] distanceSquared = new double[lm];
                        double[] shapeScore = new double[lm];
                        double[] areas = new double[lm];
                        for (int n = 0; n < lm; n++)
                        {
                            padsCheck[n] = sortPad.ElementAt(n);
                            distance[n] = ModelUtils.Distance(padsCheck[n].Center, cadItem.CenterStd);
                            distanceSquared[n] = ModelUtils.DistancePadnComponent(padsCheck[n].Center, cadItem.CenterStd, cadItem.Angle);
                            shapeScore[n] = 1 - CvInvoke.MatchShapes(new VectorOfPoint(padsCheck[n].ContourStd), new VectorOfPoint(padsCheck[0].ContourStd), Emgu.CV.CvEnum.ContoursMatchType.I3);
                            areas[n] = Math.Min(padsCheck[n].Area, padsCheck[0].Area) / Math.Max(padsCheck[n].Area, padsCheck[0].Area);
                        }
                        // component 2 pad
                        bool[] flag = new bool[lm]; 
                        for (int k = 0; k < lm; k++)
                        {
                            double disScaleSq = Math.Abs(distanceSquared[0] - distanceSquared[k]) / distanceSquared[0];
                            double disScale = Math.Abs(distance[0] - distance[k]) / distance[0];
                            if (shapeScore[k] > 0.8 && areas[k] > 0.8 &&
                                (disScaleSq < 0.3 ||
                                 disScale < 0.3))
                            {
                                flag[k] = true;
                            }
                            else
                            {
                                flag[k] = false;
                            }
                        }
                        if(flag[0])
                        {
                            if(flag[1] &&  !flag[2])
                            {
                                // is 2 pad component
                                for (int n = 0; n < 2; n++)
                                {
                                    this.Gerber.LinkPad(padsCheck[n].Pad_ID, cadItem.CadFile_ID, cadItem.CadItem_ID);
                                    cadItem.PadAttached.Add(padsCheck[n].Pad_ID);
                                }
                            }
                            else if(flag[1] && flag[2] && !flag[3])
                            { 
                                // is 3 pad component
                                for (int n = 0; n < 3; n++)
                                {
                                    this.Gerber.LinkPad(padsCheck[n].Pad_ID, cadItem.CadFile_ID, cadItem.CadItem_ID);
                                    cadItem.PadAttached.Add(padsCheck[n].Pad_ID);
                                }
                            }
                        }
                        
                    }
                }
            }
        }
        public void AutoLinkOtherComponent()
        {
            Define define = new Define();
            List<PadItem> listPadUnlink = new List<PadItem>();
            for (int i = 0; i < this.Gerber.PadItems.Count; i++)
            {
                var pad = this.Gerber.PadItems[i];
                if (pad.CadFile_ID < 0 && pad.CadItem_ID < 0)
                    listPadUnlink.Add(pad);

            }
            for (int i = 1; i < this.Cad.Count; i++)
            {
                for (int j = 0; j < this.Cad[i].CadItems.Count; j++)
                {
                    var cadItem = this.Cad[i].CadItems[j];
                    string name = Convert.ToString(cadItem.ComponentName[0]);
                    string nextName = Convert.ToString(cadItem.ComponentName[1]);
                    if ((name.ToUpper() == "R" || name.ToUpper() == "C") && "0123456789".Contains(nextName))
                        continue;
                    if (cadItem.PadAttached.Count > 0)
                        continue;
                    if (!cadItem.ComponentName.Contains("SC"))
                    {
                        cadItem.PadAttached.Clear();
                        var sortPad = listPadUnlink.OrderBy(pad => ModelUtils.Distance(pad.Center, cadItem.CenterStd));
                        int lm = 0;
                        for (int k = 0; k < listPadUnlink.Count; k++)
                        {
                            var pad = sortPad.ElementAt(k);
                            double dis = ModelUtils.Distance(pad.Center, cadItem.CenterStd);
                            if (dis < define.DPI_Std)
                            {
                                lm++;
                            }
                            else
                            {
                                break;
                            }
                        }
                        PadItem[] padsCheck = new PadItem[lm];
                        double[] distance = new double[lm];
                        double[] distanceSquared = new double[lm];
                        double[] areas = new double[lm];
                        for (int n = 0; n < lm; n++)
                        {
                            padsCheck[n] = sortPad.ElementAt(n);
                            distance[n] = ModelUtils.Distance(padsCheck[n].Center, cadItem.CenterStd);
                            distanceSquared[n] = ModelUtils.DistancePadnComponent(padsCheck[n].Center, cadItem.CenterStd, cadItem.Angle);
                            areas[n] = Math.Min(padsCheck[n].Area, padsCheck[0].Area) / Math.Max(padsCheck[n].Area, padsCheck[0].Area);
                        }
                        bool[] flag = new bool[lm];
                        for (int k = 0; k < lm; k++)
                        {
                            if (areas[k] > 0.8 && distance[k] < define.DPI_Std && distanceSquared[k] < define.DPI_Std)
                            {
                                flag[k] = true;
                            }
                            else
                            {
                                flag[k] = false;
                                break;
                            }
                        }
                        if(cadItem.ComponentName == "U905")
                        {

                        }
                        for (int n = 0; n < lm; n++)
                        {
                            if(flag[n])
                            {
                                this.Gerber.LinkPad(padsCheck[n].Pad_ID, cadItem.CadFile_ID, cadItem.CadItem_ID);
                                cadItem.PadAttached.Add(padsCheck[n].Pad_ID);
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                }
            }
        }
        public void Unlink()
        {
            this.Gerber.UnlinkPad();
            for (int i = 0; i < Cad.Count; i++)
            {
                Cad[i].UnlinkComponent();
            }
        }
        
        public List<Tuple<CadFile, CadItem>> GetSuggestLink(Rectangle rect)
        {
            Define define = new Define();
            double scale = define.DPI_Std / define.DPI_Display;
            Point center = new Point(Convert.ToInt32(scale * ( rect.X + rect.Width / 2)), Convert.ToInt32(scale * (rect.Y + rect.Height / 2)));
            List<Tuple<CadFile, CadItem>> suggest = new List<Tuple<CadFile, CadItem>>();
            for (int i = 1; i < this.Cad.Count; i++)
            {
                for (int j = 0; j < this.Cad[i].CadItems.Count; j++)
                    suggest.Add(new Tuple<CadFile, CadItem>(this.Cad[i], this.Cad[i].CadItems[j]));
            }
            var sorted = suggest.OrderBy(item => ModelUtils.Distance(center, item.Item2.CenterStd));
            List<Tuple<CadFile, CadItem>> suggestAdd = new List<Tuple<CadFile, CadItem>>(sorted.ToArray());
            suggestAdd.Add(new Tuple<CadFile, CadItem>(this.Cad[0], this.Cad[0].CadItems[0]));
            return suggestAdd;
        }
        public void LinkPadWithComponent(CadItem cadItem, Rectangle Rect)
        {
            var padInRect = this.Gerber.PadInRect(Rect, PadInRectMode.NotLink, DataType.Display);
            for (int i = 0; i < padInRect.Count; i++)
            {
                PadItem pad = this.Gerber.PadItems[padInRect[i].Item_ID];
                pad.CadFile_ID = cadItem.CadFile_ID;
                pad.CadItem_ID = cadItem.CadItem_ID;
                cadItem.PadAttached.Add(pad.Pad_ID);
            }
        }
        public void UnlinkPadWithComponent(Rectangle Rect)
        {
            var padInRect = this.Gerber.PadInRect(Rect, PadInRectMode.Linked, DataType.Display);
            for (int i = 0; i < padInRect.Count; i++)
            {
                PadItem pad = this.Gerber.PadItems[padInRect[i].Item_ID];
                if(pad.CadFile_ID > 0)
                {
                    this.Cad[pad.CadFile_ID].CadItems[pad.CadItem_ID].PadAttached.Remove(pad.Pad_ID);
                }
                pad.CadFile_ID = -1;
                pad.CadItem_ID = -1;
            }
        }
        #region PCB
        public PCBItem DefinePCB(Rectangle Rect, DataType dataType = DataType.Display)
        {
            var pcb = this.Gerber.DefinePCB(Rect, dataType);
            if (pcb.PCB_ID >= 0)
            {
                this.Gerber.PCBItems.Add(pcb);
                return pcb;
            }
            else
                return null;
        }
        private double ScoresDirectPCB(Rectangle Bound, int Width, int Height, double Angle)
        {
            double scale = 100.0;
            double x = (Bound.X + Bound.Width / 2) / scale;
            double y = (Bound.Y + Bound.Height / 2) / scale;
            double w = Width / scale;
            double h = Height / scale;

            double dis = 0;
            double x1 = 0;
            double y1 = 0;
            double x2 = 0;
            double y2 = 0;
            switch (Angle)
            {
                case 0:
                    x1 = 0;
                    y1 = 0;
                    x2 = x;
                    y2 = y;
                    break;
                case 90:
                    x1 = w;
                    y1 = 0;
                    x2 = w - x;
                    y2 = y;
                    break;
                case 180:
                    x1 = w;
                    y1 = h;
                    x2 = w - x;
                    y2 = h - y;
                    break;
                case 270:
                    x1 = 0;
                    y1 = h;
                    x2 = x;
                    y2 = h - y;
                    break;
                default:
                    break;
            }
            dis = Math.Pow(x2 - x1, 2) + Math.Pow(y2 - y1, 2);
            return dis;
        }
        public void SortPCB()
        {
            int w = this.Gerber.ImageWidth;
            int h = this.Gerber.ImageHeight;
            double angle = this.Gerber.GerberAngle;
            var sorted = this.Gerber.PCBItems.OrderBy(x => ScoresDirectPCB(x.Bounding, w, h, angle)).ToArray();
            this.Gerber.PCBItems.Clear();
            this.Gerber.PCBItems.AddRange(sorted);
            for (int i = 0; i < this.Gerber.PCBItems.Count; i++)
            {
                var pcb = this.Gerber.PCBItems[i];
                pcb.PCB_ID = i;
                pcb.IDFromIT = i;
                for (int j = 0; j < pcb.PadItems.Count; j++)
                {
                    int idPad = pcb.PadItems[j];
                    this.Gerber.PadItems[idPad].PCB_ID = pcb.PCB_ID;
                }
            }
        }
        public List<Tuple<Rectangle, double>> FindPCB(Rectangle Rect, double ScaleUpSpeed, DataType dataType = DataType.Display)
        {
            var pcb = Gerber.FindPCB(Rect, Emgu.CV.CvEnum.TemplateMatchingType.CcoeffNormed, ScaleUpSpeed: ScaleUpSpeed, DataType: dataType);
            return pcb;
        }
        #endregion
        public void AutoCopyLink(Rectangle pcbRef, List<Tuple<Rectangle, double>> pcbFound, int InflateX = 20, int InflateY = 20, DataType dataType = DataType.Display)
        {
            // define PCB & adjust Bouding
            List<PCBItem> newPCB = new List<PCBItem>();
            List<double> anglePCB = new List<double>();
            for (int i = 0; i < pcbFound.Count; i++)
            {
                Rectangle bound = pcbFound[i].Item1;
                anglePCB.Add(pcbFound[i].Item2);
                bound = this.Gerber.AutoAdjustFoundPCB(pcbRef, bound, anglePCB[i], dataType: dataType);
                bound.Inflate(5, 5);
                var pcb = DefinePCB(bound, dataType);
                newPCB.Add(pcb);
            }
            // find cad in PCB bouding
            List<int> cadIDLinkedInRect = this.Gerber.CadFileInRect(pcbRef, dataType);
            for (int i = 0; i < this.Cad.Count; i++)
            {
                if (this.Cad[i].SelectItems(pcbRef, dataType).Count > 0 && !cadIDLinkedInRect.Contains(i))
                {
                    cadIDLinkedInRect.Add(i);
                }
            }
            cadIDLinkedInRect.Remove(0);
            // Auto copy PCB
            List<CadFile[]> newCad = new List<CadFile[]>();
            int newIdCad = this.Cad.Count;
            // get all pad of pcb reference
            var padInPCBRef = this.Gerber.PadInRect(pcbRef, PadInRectMode.Linked, dataType);
            for (int i = 0; i < newPCB.Count; i++)
            {
                // auto define CadFile & link Pad with cad file
                CadFile[] newCadFile = new CadFile[cadIDLinkedInRect.Count];
                Rectangle bound = newPCB[i].Bounding;
                Point shift = this.Gerber.GetShiftXYPCB(pcbRef, bound, dataType);
                Point ct = new Point(bound.X + bound.Width / 2, bound.Y + bound.Height / 2);
                double angle = anglePCB[i];
                for (int j = 0; j < cadIDLinkedInRect.Count; j++)
                {
                    int idCad = cadIDLinkedInRect[j];
                    newCadFile[j] = this.Cad[idCad].Copy();
                    newCadFile[j].SetCadID(newIdCad);
                    newCadFile[j].Shift(shift.X, shift.Y);
                    newCadFile[j].Rotation(angle, Center: ct, dataType: dataType);
                    newIdCad++;
                }
                this.Cad.AddRange(newCadFile);
                newCad.Add(newCadFile);

                // link Cad with Pad
                foreach (CadFile cadFile in newCadFile)
                {
                    foreach (var cadItem in cadFile.CadItems)
                    {
                        for (int j = 0; j < cadItem.PadAttached.Count; j++)
                        {
                            int idPadLink = cadItem.PadAttached[j];
                            int newId = this.Gerber.GetPadIDOfNewPCB(idPadLink, shift.X, shift.Y, ct, angle, dataType: dataType);
                            cadItem.PadAttached[j] = newId;
                            this.Gerber.PadItems[newId].CadFile_ID = cadFile.CadFile_ID;
                            this.Gerber.PadItems[newId].CadItem_ID = cadItem.CadItem_ID;
                        }
                    }
                }
                // find & link cad undefine of PCB
                for (int n = 0; n < padInPCBRef.Count; n++)
                {
                    int idPad = padInPCBRef[n].Item_ID;
                    PadItem pad =  this.Gerber.PadItems[idPad];
                    if(pad.CadFile_ID == 0 && pad.CadItem_ID == 0)
                    {
                        int newId = this.Gerber.GetPadIDOfNewPCB(idPad, shift.X, shift.Y, ct, angle, dataType: dataType);
                        this.Gerber.PadItems[newId].CadFile_ID = 0;
                        this.Gerber.PadItems[newId].CadItem_ID = 0;
                        this.Cad[0].CadItems[0].PadAttached.Add(newId);
                        Console.WriteLine(newId);
                    }
                }
            }
        }
        public void RemovePCB(int PCB_ID)
        {
            this.Gerber.RemovePCB(PCB_ID);
        }
        public void AddNewGroup(string GroupName)
        {
            this.Gerber.AddNewGroup(GroupName);
        }
        public void UpdateItems2Group(string GroupName, List<int> IdPadItems)
        {
            this.Gerber.UpdateItems2Group(GroupName, IdPadItems);
        }
        public void RemoveGroup(string GroupName)
        {
            this.Gerber.RemoveGroup(GroupName);
        }
        public List<int> FindTheSamePadItems(List<int> IDPadRef, FindPadMode findPadMode)
        {
            List<int> theSamePad = new List<int>();
            List<PadItem> padRef = new List<PadItem>();
            List<PadItem> padOutRef = new List<PadItem>();
            List<string> componentCode = new List<string>();
            List<string> componentName = new List<string>();
            // find pad cad information
            for (int i = 0; i < this.Gerber.PadItems.Count; i++)
            {
                if (IDPadRef.Contains(this.Gerber.PadItems[i].Pad_ID))
                {
                    var pad = this.Gerber.PadItems[i];
                    if(pad.CadFile_ID >= 0 && pad.CadItem_ID >= 0)
                    {
                        CadItem cad = this.Cad[pad.CadFile_ID].CadItems[pad.CadItem_ID];
                        componentCode.Add(cad.ComponentCode);
                        componentName.Add(cad.ComponentName);
                        padRef.Add(pad);
                    }
                    
                }
            }

            // find pad the same cad

            for (int i = 0; i < this.Gerber.PadItems.Count; i++)
            {
                if (!IDPadRef.Contains(this.Gerber.PadItems[i].Pad_ID))
                {
                    var pad = this.Gerber.PadItems[i];
                    if (pad.CadFile_ID >= 0 && pad.CadItem_ID >= 0)
                    {
                        CadItem cad = this.Cad[pad.CadFile_ID].CadItems[pad.CadItem_ID];
                        if(findPadMode == FindPadMode.TheSameComponentName)
                        {
                            if(componentName.Contains(cad.ComponentName))
                            {
                                theSamePad.Add(pad.Pad_ID);
                            }
                            
                        }
                        if(findPadMode == FindPadMode.TheSameComponentCode)
                        {
                            if (componentCode.Contains(cad.ComponentCode))
                            {
                                theSamePad.Add(pad.Pad_ID);
                            }
                        }
                        if (findPadMode == FindPadMode.TheSamePad)
                        {
                            if (componentCode.Contains(cad.ComponentCode))
                            {
                                padOutRef.Add(pad);
                            }
                        }
                    }

                }
            }
            if (findPadMode == FindPadMode.TheSameComponentCode || findPadMode == FindPadMode.TheSameComponentName)
                return theSamePad;

            foreach (var padOut in padOutRef)
            {
                if (!theSamePad.Contains(padOut.Pad_ID) && !IDPadRef.Contains(padOut.Pad_ID))
                {
                    foreach (var padIn in padRef)
                    {
                        if (this.Gerber.TheSamePad(padOut, padIn))
                        {
                            theSamePad.Add(padOut.Pad_ID);
                            break;
                        }
                    }
                }
            }
            return theSamePad;
        }
        public void UncheckPad(List<int> IDPad)
        {
            this.Gerber.UncheckPad(IDPad);
        }
        public void ActiveCheckPad(List<int> IDPad)
        {
            this.Gerber.ActiveCheckPad(IDPad);
        }
        public Model Clone()
        {
            string s = JsonConvert.SerializeObject(this);
            return JsonConvert.DeserializeObject<Model>(s);
        }
    }
}
