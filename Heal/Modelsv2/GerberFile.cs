﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System.Threading;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Xml;



namespace SPI_AOI.Modelsv2
{
    public class GerberFile : ImportFile
    {
        public int Gerber_ID { get; set; }
        public bool Visible { get; set; }
        public int ImageWidth { get; set; }
        public int ImageHeight { get; set; }
        public double GerberAngle { get; set; }
        public List <PadItem> PadItems { get; set; }
        public List<TensionItem> TensionItems { get; set; }
        public List<PCBItem> PCBItems { get; set; }
        public List<FovItem> FOVItems { get; set; }
        public List<MarkItem> MarkItems { get; set; }
        public List<GroupItem> GroupItems { get; set; }
        public GerberFile()
        {
            this.Gerber_ID = 0;
            this.Color = Color.FromArgb(255, 0, 0);
            this.Visible = true;
            // image size display
            this.ImageHeight = -1;
            this.ImageWidth = -1;
            this.PadItems = new List<PadItem>();
            this.TensionItems = new List<TensionItem>();
            this.FOVItems = new List<FovItem>();
            this.PCBItems = new List<PCBItem>();
            this.MarkItems = new List<MarkItem>();
            this.GroupItems = new List<GroupItem>();
        }
        public bool LoadFile(string FilePath)
        {
            Define define = new Define();
            try
            {
                FileInfo fi = new FileInfo(FilePath);
                var result = Utils.GerberUtils.Render(FilePath, define.DPI_Std);
                if (result.Status == Utils.ActionStatus.Successfully)
                {
                    GetPads(result.GerberImage, define.DPI_Std, define.DPI_Display);
                    this.ImageWidth = Convert.ToInt32(result.GerberImage.Width);
                    this.ImageHeight = Convert.ToInt32(result.GerberImage.Height);
                    this.GerberAngle = this.GetGerberAngle(result.GerberImage);
                }
                this.FileName = fi.Name;
                this.FilePath = fi.FullName;
                this.Time = DateTime.Now;
                
                result.Dispose();
                result = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
            catch (Exception)
            {
                return false;
            }
            this.PCBItems.Clear();
            this.FOVItems.Clear();
            this.MarkItems.Clear();
            this.GroupItems.Clear();
            return true;
        }
        public double GetGerberAngle(Image<Gray, byte> ImageGerber)
        {
            double angle = 0;
            int width = ImageGerber.Width;
            int height = ImageGerber.Height;
            int n = 20;
            /*  y1 = h / 2 + ((h / 2) / n) * (0.6 * n)
                y2 = h / 2 + ((h / 2) / n) * (n - 1)
                x1 = w / 2 - ((w / 2) / n) * (0.6 * n)
                x2 = w / 2 + ((w / 2) / n) * (0.6 * n) */
            double x1 = (double)width / 2 - (((double)width / 2) / n) * (0.6 * n);
            double x2 = (double)width / 2 + (((double)width / 2) / n) * (0.6 * n);
            double y1 = (double)height / 2 + (((double)height / 2) / n) * (0.6 * n);
            double y2 = (double)height / 2 + (((double)height / 2) / n) * (n - 1);
            Point ct = new Point(width / 2, height / 2);
            Point p1 = new Point(Convert.ToInt32(x1), Convert.ToInt32(y1));
            Point p2 = new Point(Convert.ToInt32(x2), Convert.ToInt32(y1));
            Point p3 = new Point(Convert.ToInt32(x2), Convert.ToInt32(y2));
            Point p4 = new Point(Convert.ToInt32(x1), Convert.ToInt32(y2));
            double maxCount = 0;
            for (int i = 0; i < 360; i+= 90)
            {
                Point _p1 = ModelUtils.PointRotate(p1, ct, i * Math.PI / 180.0);
                Point _p2 = ModelUtils.PointRotate(p2, ct, i * Math.PI / 180.0);
                Point _p3 = ModelUtils.PointRotate(p3, ct, i * Math.PI / 180.0);
                Point _p4 = ModelUtils.PointRotate(p4, ct, i * Math.PI / 180.0);
                int[] listX = new int[] { _p1.X, _p2.X, _p3.X, _p4.X};
                int[] listY = new int[] { _p1.Y, _p2.Y, _p3.Y, _p4.Y};
                int minX = listX.Min();
                int maxX = listX.Max();
                int minY = listY.Min();
                int maxY = listY.Max();
                int x = minX;
                int y = minY;
                int w = maxX - minX;
                int h = maxY - minY;
                Rectangle ROI = new Rectangle(x, y, w, h);
                ImageGerber.ROI = ROI;
                int count = CvInvoke.CountNonZero(ImageGerber);
                ImageGerber.ROI = Rectangle.Empty;
                if(count > maxCount)
                {
                    angle = i;
                }
            }
            return angle;
        }
        public void GetPads (Image<Gray, byte> image, float DPI_Std, float DPI_Display)
        {
            this.PadItems.Clear();
            using (VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint())
            using (Mat hierarchy = new Mat())
            {
                CvInvoke.FindContours(image, contours, hierarchy, Emgu.CV.CvEnum.RetrType.Tree, Emgu.CV.CvEnum.ChainApproxMethod.ChainApproxSimple);
                int count = 0;
                for (int i = 0; i < contours.Size; i++)
                {
                    Point[] padStd = contours[i].ToArray();
                    Point[] padDisplay = new Point[padStd.Length];
                    Point[] padStdAdjust = new Point[padStd.Length];
                    float scaleDisplay = DPI_Display / DPI_Std;
                    for (int j = 0; j < padStd.Length; j++)
                    {
                        padDisplay[j] = new Point(Convert.ToInt32(padStd[j].X * scaleDisplay), Convert.ToInt32(padStd[j].Y * scaleDisplay));
                        padStdAdjust[j] = new Point(padStd[j].X, padStd[j].Y);
                    }
                    using (var cnt = new VectorOfPoint(padStd))
                    {
                        Moments mm = CvInvoke.Moments(cnt);
                        if (mm.M00 == 0)
                            continue;
                        Point ctCnt = new Point(Convert.ToInt32(mm.M10 / mm.M00), Convert.ToInt32(mm.M01 / mm.M00));
                        Rectangle bound = CvInvoke.BoundingRectangle(cnt);
                        double area = ImageProcessingUtils.ContourArea(cnt);
                        PadItem pad = new PadItem();
                        pad.Area = area;
                        pad.Pad_ID = count;
                        pad.Bouding = bound;
                        pad.Center = ctCnt;
                        pad.CenterRotate = new Point(image.Width / 2, image.Height / 2);
                        pad.ContourDisplay = padDisplay;
                        pad.ContourStd = padStd;
                        pad.ContourStdAdjust = padStdAdjust;
                        this.PadItems.Add(pad);
                    }
                    count++;
                }
            }
            FOVItems.Clear();
        }
        public void ConvertRealPad(double Real_DPI, Size FOV)
        {
            Define define = new Define();
            double scale = Real_DPI / define.DPI_Std;
            foreach (var padItem in this.PadItems)
            {
                padItem.BoudingReal = new Rectangle(
                    Convert.ToInt32(padItem.Bouding.X * scale),
                    Convert.ToInt32(padItem.Bouding.Y * scale),
                    Convert.ToInt32(padItem.Bouding.Width * scale),
                    Convert.ToInt32(padItem.Bouding.Height * scale)
                    );
                padItem.CenterReal = new Point(
                    Convert.ToInt32(padItem.Center.X * scale),
                    Convert.ToInt32(padItem.Center.Y * scale)
                    );
                padItem.ContourReal = new Point[padItem.ContourStd.Length];
                for (int i = 0; i < padItem.ContourStd.Length; i++)
                {
                    var p = padItem.ContourStd[i];
                    padItem.ContourReal[i] = new Point(Convert.ToInt32(p.X * scale), Convert.ToInt32(p.Y * scale));
                }
                
                padItem.AreaReal = ImageProcessingUtils.ContourArea(padItem.ContourReal);
            }
            foreach (var item in this.TensionItems)
            {
                item.TensionPointReal = new Point(
                    Convert.ToInt32(item.TensionPointStd.X * scale),
                    Convert.ToInt32(item.TensionPointStd.Y * scale)
                    );
            }
            foreach (var item in this.FOVItems)
            {
                item.RealAnchor = new Point(
                    Convert.ToInt32(item.Anchor.X * scale),
                    Convert.ToInt32(item.Anchor.Y * scale)
                    );
                item.RealROIGerber = new Rectangle(
                    Convert.ToInt32(item.ROIGerber.X * scale),
                    Convert.ToInt32(item.ROIGerber.Y * scale),
                    Convert.ToInt32(item.ROIGerber.Width * scale), 
                    Convert.ToInt32(item.ROIGerber.Height * scale)
                    );

                foreach (var idPad in item.PadItems)
                {
                    var padItem = this.PadItems[idPad];
                    padItem.ContourOnFOV = new Point[padItem.ContourReal.Length];
                    int subX = item.RealAnchor.X - FOV.Width / 2;
                    int subY = item.RealAnchor.Y - FOV.Height / 2;
                    for (int j = 0; j < padItem.ContourReal.Length; j++)
                    {
                        var p = padItem.ContourReal[j];
                        padItem.ContourOnFOV[j] = new Point(p.X - subX, p.Y - subY);
                    }
                }
                bool isCheck = false;
                for (int i = 0; i < item.PadItems.Count; i++)
                {
                    int idPad = item.PadItems[i];
                    var pad = this.PadItems[i];
                    if(pad.Check)
                    {
                        isCheck = true;
                    }
                }
                item.Check = isCheck;
            }
        }
        public void Rotate(double Angle)
        {
            Angle = Angle * Math.PI / 180.0;
            Define define = new Define();
            double scale = define.DPI_Display / define.DPI_Std;
            int xmin = 65535;
            int xmax = -65535;
            int ymin = 65535;
            int ymax = -65535;
            int ext = define.ExtImage;
            foreach (var item in PadItems)
            {
                var center = new Point(this.ImageWidth / 2, this.ImageHeight / 2);
                var padDisplay = item.ContourDisplay;
                var padStd = item.ContourStd;
                var padStdAdjust = item.ContourStdAdjust;
                for (int i = 0; i < padStd.Length; i++)
                {
                    padStd[i] = ModelUtils.PointRotate(padStd[i], center, Angle);
                    padStdAdjust[i] = ModelUtils.PointRotate(padStdAdjust[i], center, Angle);
                    var centerDisplay = new Point(Convert.ToInt32(center.X * define.DPI_Display / define.DPI_Std), Convert.ToInt32(center.Y * define.DPI_Display / define.DPI_Std));
                    padDisplay[i] = ModelUtils.PointRotate(padDisplay[i], centerDisplay, Angle);
                    xmin = padStd[i].X < xmin ? padStd[i].X : xmin;
                    ymin = padStd[i].Y < ymin ? padStd[i].Y : ymin;
                    xmax = padStd[i].X > xmax ? padStd[i].X : xmax;
                    ymax = padStd[i].Y > ymax ? padStd[i].Y : ymax;
                }
            }
            int x = -xmin + ext;
            int y = -ymin + ext;
            foreach (var item in PadItems)
            {
                var padDisplay = item.ContourDisplay;
                var padStd = item.ContourStd;
                var padStdAdjust = item.ContourStdAdjust;
                for (int i = 0; i < padStd.Length; i++)
                {
                    padStd[i].X += x;
                    padStd[i].Y += y;
                    padStdAdjust[i].X += x;
                    padStdAdjust[i].Y += y;
                    padDisplay[i].X += Convert.ToInt32(x * scale);
                    padDisplay[i].Y += Convert.ToInt32(y * scale);
                }
                using (var cnt = new VectorOfPoint(padStd))
                {
                    Moments mm = CvInvoke.Moments(cnt);
                    if (mm.M00 == 0)
                        continue;
                    Point ctCnt = new Point(Convert.ToInt32(mm.M10 / mm.M00), Convert.ToInt32(mm.M01 / mm.M00));
                    Rectangle bound = CvInvoke.BoundingRectangle(cnt);
                    double area = ImageProcessingUtils.ContourArea(cnt);
                    item.Area = area;
                    item.Bouding = bound;
                    item.Center = ctCnt;
                }
            }
            this.ImageWidth = xmax - xmin + 2 * ext;
            this.ImageHeight = ymax - ymin + 2 * ext;
            double a = Angle * 180.0 / Math.PI;
            if (a < 0)
                a += 360;
            this.GerberAngle += a;
            this.GerberAngle = GerberAngle % 360;
            Console.WriteLine(this.GerberAngle);
        }
        public void Crop(Rectangle Rect, DataType DataType = DataType.Display)
        {
            if (Rect == null || Rect == Rectangle.Empty)
                return;
            Define define = new Define();
            double scale = define.DPI_Std / define.DPI_Display;
            Rectangle rectStd = Rect;
            if(DataType == DataType.Display)
                rectStd = new Rectangle(Convert.ToInt32(Rect.X * scale), Convert.ToInt32(Rect.Y * scale), Convert.ToInt32(Rect.Width * scale), Convert.ToInt32(Rect.Height * scale));
            int i = 0;
            int limit = PadItems.Count;
            int ext = define.ExtImage;
            int xl = 65535;
            int yl = 65535;
            int xh = 0;
            int yh = 0;
            for (; i < limit; )
            {
                var item = PadItems[i];
                if (!rectStd.Contains(item.Bouding))
                {
                    PadItems.Remove(item);
                    limit = PadItems.Count;
                }
                else
                {
                    for (int j = 0; j < item.ContourStd.Length; j++)
                    {
                        var p = item.ContourStd[j];
                        xl = p.X < xl ? p.X : xl;
                        yl = p.Y < yl ? p.Y : yl;
                        xh = p.X > xh ? p.X : xh;
                        yh = p.Y > yh ? p.Y : yh;
                    }
                    i++;
                }
            }
            this.ImageWidth = Convert.ToInt32((xh - xl + 2 * ext));
            this.ImageHeight = Convert.ToInt32((yh - yl + 2 * ext));
            int count = 0;
            foreach (var item in PadItems)
            {
                Point[] cntDisplay = item.ContourDisplay;
                Point[] cntStd = item.ContourStd;
                Point[] cntAdjust = item.ContourStdAdjust;

                for (int j = 0; j < cntStd.Length; j++)
                {
                    cntStd[j].X -= Convert.ToInt32(xl - ext);
                    cntStd[j].Y -= Convert.ToInt32(yl - ext);
                    cntAdjust[j].X -= Convert.ToInt32(xl - ext);
                    cntAdjust[j].Y -= Convert.ToInt32(yl - ext);
                    cntDisplay[j].X -= Convert.ToInt32((xl - ext) / scale);
                    cntDisplay[j].Y -= Convert.ToInt32((yl - ext) / scale);
                }
                using (var cnt = new VectorOfPoint(item.ContourStd))
                {
                    Moments mm = CvInvoke.Moments(cnt);
                    if (mm.M00 == 0)
                        continue;
                    Point ctCnt = new Point(Convert.ToInt32(mm.M10 / mm.M00), Convert.ToInt32(mm.M01 / mm.M00));
                    Rectangle bound = CvInvoke.BoundingRectangle(cnt);
                    double area = ImageProcessingUtils.ContourArea(cnt);
                    item.Area = area;
                    item.Pad_ID = count;
                    item.Bouding = bound;
                    item.Center = ctCnt;
                    item.CenterRotate = new Point((xh - xl + 2 * ext) / 2, (xh - xl + 2 * ext) / 2);
                }
                count++;
            }
            
        }
        public List<ItemSelected> PadInRect(Rectangle Rect, PadInRectMode mode, DataType DataType = DataType.Display)
        {
            List<ItemSelected> listPadInRect = new List<ItemSelected>();
            if (Rect == null || Rect == Rectangle.Empty)
                return listPadInRect;
            Define define = new Define();
            double scale = define.DPI_Std / define.DPI_Display;
            Rectangle rectStd = Rect;
            if (DataType == DataType.Display)
                rectStd = new Rectangle(Convert.ToInt32(Rect.X * scale), Convert.ToInt32(Rect.Y * scale), Convert.ToInt32(Rect.Width * scale), Convert.ToInt32(Rect.Height * scale));
            List<ItemSelected> listPadLinked = new List<ItemSelected>();
            List<ItemSelected> listPadNotLink = new List<ItemSelected>();
            foreach (var item in this.PadItems)
            {
                var p = item.Center;
                if (rectStd.Contains(p))
                {
                    ItemSelected si = new ItemSelected();
                    si.Type = ItemType.Gerber;
                    si.Item_ID = item.Pad_ID;
                    si.Perent_ID = this.Gerber_ID;
                    if (mode == PadInRectMode.Linked && item.CadFile_ID >= 0)
                    {
                        listPadLinked.Add(si);
                    }
                    else if (mode == PadInRectMode.NotLink && item.CadFile_ID < 0)
                    {
                        listPadNotLink.Add(si);
                    }
                    else if (mode == PadInRectMode.All)
                    {
                        listPadInRect.Add(si);
                    }
                }
            }
            listPadInRect.AddRange(listPadLinked);
            listPadInRect.AddRange(listPadNotLink);
            return listPadInRect;
        }
        public Point GetCenterOfPadSelected(List<int> ListSelectID, DataType DataType = DataType.Standard)
        {
            Define define = new Define();
            double scale = define.DPI_Display / define.DPI_Std;
            Point ct = new Point(-1, -1);
            if(ListSelectID.Count > 0)
            {
                long x = 0;
                long y = 0;
                for (int i = 0; i < this.PadItems.Count; i++)
                {
                    if (ListSelectID.Contains(i))
                    {
                        x += this.PadItems[i].Center.X;
                        y += this.PadItems[i].Center.Y;
                    }
                }
                x = x / ListSelectID.Count;
                y = y / ListSelectID.Count;
                if(DataType == DataType.Standard)
                    ct = new Point(Convert.ToInt32(x), Convert.ToInt32(y));
                else
                    ct = new Point(Convert.ToInt32(x * scale), Convert.ToInt32(y * scale));
            }
            return ct;
        }
        public void LinkPad(int PadID, int CadFile_ID, int CadItem_ID)
        {
            var pad = this.PadItems[PadID];
            pad.CadFile_ID = CadFile_ID;
            pad.CadItem_ID = CadItem_ID;
        }
        public void UnlinkPad(int PadID = -1)
        {
            if(PadID == -1)
            {
                foreach (var item in PadItems)
                {
                    item.CadFile_ID = -1;
                    item.CadItem_ID = -1;
                }
            }
            else
            {
                var pad = this.PadItems[PadID];
                pad.CadFile_ID = -1;
                pad.CadItem_ID = -1;
            }
        }
        public void RemovePad(List<int> ListPadID)
        {
            for (int i = 0; i < ListPadID.Count; i++)
            {
                this.PadItems[ListPadID[i]].Visible = false;
            }
        }
        public List<int> CadFileInRect(Rectangle Rect, DataType dataType)
        {
            List<int> listCadId = new List<int>();
            var padInRect = PadInRect(Rect, PadInRectMode.Linked, dataType);
            for (int i = 0; i < padInRect.Count; i++)
            {
                int padId = padInRect[i].Item_ID;
                int cadId = this.PadItems[padId].CadFile_ID;
                if (!listCadId.Contains(cadId))
                {
                    listCadId.Add(cadId);
                }
            }
            return listCadId;
        }
        public List<Tuple<Rectangle, double>> FindPCB(Rectangle Rect, Emgu.CV.CvEnum.TemplateMatchingType templateMatchingType = Emgu.CV.CvEnum.TemplateMatchingType.CcoeffNormed, double MatchingScale = 50, DataType DataType = DataType.Display, double ScaleUpSpeed = 2)
        {
            Define define = new Define();
            double scale = define.DPI_Display / define.DPI_Std;
            List<Tuple<Rectangle, double>> pcb = new List<Tuple<Rectangle, double>>();
            double scaleUpSpeed = 1 / ScaleUpSpeed;
            Rectangle ROI_PCB = Rect;
            if(DataType == DataType.Standard)
            {
                ROI_PCB = new Rectangle(
                    Convert.ToInt32(ROI_PCB.X * scale), 
                    Convert.ToInt32(ROI_PCB.Y * scale),
                    Convert.ToInt32(ROI_PCB.Width * scale),
                    Convert.ToInt32(ROI_PCB.Height * scale));
            }
            Image<Gray, byte> imageGerber = new Image<Gray, byte>(Convert.ToInt32(this.ImageWidth * scale), Convert.ToInt32(this.ImageHeight * scale));
            
            using (VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint())
            {
                for (int i = 0; i < this.PadItems.Count; i++)
                {
                    var pad = this.PadItems[i];
                    if (pad.Visible)
                    {
                        contours.Push(new VectorOfPoint(pad.ContourDisplay));
                    }
                }
                CvInvoke.DrawContours(imageGerber, contours, -1, new MCvScalar(255), -1);
            }
            // resize & release memory
            Image<Gray, byte> imageGerberResize = imageGerber.Resize(scaleUpSpeed, Emgu.CV.CvEnum.Inter.Linear);
            
            ROI_PCB = new Rectangle(
                    Convert.ToInt32(ROI_PCB.X * scaleUpSpeed),
                    Convert.ToInt32(ROI_PCB.Y * scaleUpSpeed),
                    Convert.ToInt32(ROI_PCB.Width * scaleUpSpeed),
                    Convert.ToInt32(ROI_PCB.Height * scaleUpSpeed));
            imageGerberResize.ROI = ROI_PCB;
            Image<Gray, byte> imagePCB = imageGerberResize.Copy();
            imageGerberResize.ROI = Rectangle.Empty;
            CvInvoke.Rectangle(imageGerberResize, ROI_PCB, new MCvScalar(0), -1);
            using (Mat result = new Mat())
            {
                for (int i = 0; i < 4; i++)
                {
                    if(i != 0)
                        imagePCB = ImageProcessingUtils.ImageRotation(imagePCB, new Point(imagePCB.Width / 2, imagePCB.Height / 2), 90 * Math.PI / 180.0);
                    CvInvoke.MatchTemplate(imageGerberResize, imagePCB, result, templateMatchingType);
                    var data = result.GetData();
                    for (int y = 0; y < result.Height; y++)
                    {
                        for (int x = 0; x < result.Width; x++)
                        {
                            float value = (float)data.GetValue(y, x);
                            if (value > MatchingScale / 100)
                            {
                                Rectangle rect = new Rectangle(new Point(x, y), imagePCB.Size);
                                bool intersect = false;
                                for (int r = 0; r < pcb.Count; r++)
                                {
                                    if(pcb[r].Item1.IntersectsWith(rect))
                                    {
                                        var rectIntersect = Rectangle.Intersect(pcb[r].Item1, rect);
                                        if(rectIntersect.Width * rectIntersect.Height > imagePCB.Width * imagePCB.Height  * 20 / 100.0)
                                            intersect = true;
                                    }
                                }
                                for (int r = 0; r < this.PCBItems.Count; r++)
                                {
                                    Rectangle boundPCB = this.PCBItems[r].Bounding;
                                    boundPCB = new Rectangle(
                                            Convert.ToInt32(boundPCB.X * scale),
                                            Convert.ToInt32(boundPCB.Y * scale),
                                            Convert.ToInt32(boundPCB.Width * scale),
                                            Convert.ToInt32(boundPCB.Height * scale));

                                    boundPCB = new Rectangle(
                                            Convert.ToInt32(boundPCB.X * scaleUpSpeed),
                                            Convert.ToInt32(boundPCB.Y * scaleUpSpeed),
                                            Convert.ToInt32(boundPCB.Width * scaleUpSpeed),
                                            Convert.ToInt32(boundPCB.Height * scaleUpSpeed));
                                    if (boundPCB.IntersectsWith(rect))
                                    {
                                        var rectIntersect = Rectangle.Intersect(boundPCB, rect);
                                        if (rectIntersect.Width * rectIntersect.Height > imagePCB.Width * imagePCB.Height * 20 / 100.0)
                                            intersect = true;
                                    }
                                }
                                if(!intersect)
                                {
                                    
                                    pcb.Add(new Tuple<Rectangle, double>(rect, i * 90));
                                }
                            }
                        }
                    }
                }
            }
            imageGerberResize.Dispose();
            imageGerberResize = null;
            imagePCB.Dispose();
            imagePCB = null;
            imageGerber.Dispose();
            imageGerber = null;
            for (int i = 0; i < pcb.Count; i++)
            {
                // convert to std dpi
                Rectangle rect = pcb[i].Item1;
                rect.X = Convert.ToInt32((rect.X / scaleUpSpeed));
                rect.Y = Convert.ToInt32((rect.Y / scaleUpSpeed));
                rect.Width = Convert.ToInt32((rect.Width / scaleUpSpeed));
                rect.Height = Convert.ToInt32((rect.Height / scaleUpSpeed));
                if (DataType == DataType.Standard)
                {
                    rect = new Rectangle(
                        Convert.ToInt32(rect.X / scale),
                        Convert.ToInt32(rect.Y / scale),
                        Convert.ToInt32(rect.Width / scale),
                        Convert.ToInt32(rect.Height / scale));
                }
                //--------------
                pcb[i] = new Tuple<Rectangle, double>(rect, pcb[i].Item2);
            }
            return pcb;
        }

        public Point GetShiftXYPCB(Rectangle BoudingPCB1, Rectangle BoudingPCB2, DataType dataType = DataType.Display)
        {
            Define define = new Define();
            double scale = define.DPI_Display / define.DPI_Std;
            Point ct1 = new Point(BoudingPCB1.X + BoudingPCB1.Width / 2, BoudingPCB1.Y + BoudingPCB1.Height / 2);
            Point ct2 = new Point(BoudingPCB2.X + BoudingPCB2.Width / 2, BoudingPCB2.Y + BoudingPCB2.Height / 2);
            int x = ct2.X - ct1.X;
            int y = ct2.Y - ct1.Y;
            if(dataType == DataType.Display)
            {
                x = Convert.ToInt32((double)x / scale);
                y = Convert.ToInt32((double)y / scale);
            }
            return new Point(x, y);
        }
        public Rectangle AutoAdjustFoundPCB(Rectangle BoudingRef, Rectangle BoudingFound, double Angle, int X = 20, int Y = 20, DataType dataType = DataType.Display)
        {
            Define define = new Define();
            double scale = define.DPI_Display / define.DPI_Std;
            int imageWidth = Convert.ToInt32(this.ImageWidth * scale);
            int imageHeight = Convert.ToInt32(this.ImageHeight * scale);
            Image<Gray, byte> imageGB = new Image<Gray, byte>(imageWidth, imageHeight);
            using (VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint())
            {
                for (int i = 0; i < this.PadItems.Count; i++)
                {
                    contours.Push(new VectorOfPoint(this.PadItems[i].ContourDisplay));
                }
                CvInvoke.DrawContours(imageGB, contours, -1, new MCvScalar(255), -1);
            }
            Rectangle ROIRef = BoudingRef;
            Rectangle ROIFound = BoudingFound;
            if (dataType == DataType.Standard)
            {
                ROIRef = new Rectangle(Convert.ToInt32(ROIRef.X * scale), Convert.ToInt32(ROIRef.Y * scale), Convert.ToInt32(ROIRef.Width * scale), Convert.ToInt32(ROIRef.Height * scale));
                ROIFound = new Rectangle(Convert.ToInt32(ROIFound.X * scale), Convert.ToInt32(ROIFound.Y * scale), Convert.ToInt32(ROIFound.Width * scale), Convert.ToInt32(ROIFound.Height * scale));
            }

            
            // found limit bouding


            int width = Math.Min(ROIRef.Width, ROIFound.Width);
            int height = Math.Min(ROIRef.Height, ROIFound.Height);
            int subXRef = ROIRef.Width - width;
            int subYRef = ROIRef.Height - height; 
            int subXFound = ROIFound.Width - width;
            int subYFound = ROIFound.Height - height;

            ROIRef.X += subXRef;
            ROIRef.Y += subYRef;
            ROIRef.Width = width;
            ROIRef.Height = height;
            ROIFound.X += subXFound;
            ROIFound.Y += subYFound;
            ROIFound.Width = width;
            ROIFound.Height = height;

            // copy image
            imageGB.ROI = ROIRef;
            Image<Gray, byte> ImageRef = imageGB.Copy();
            imageGB.ROI = Rectangle.Empty;
            imageGB.ROI = ROIFound;
            Image<Gray, byte> ImageFound = imageGB.Copy();
            if (Angle != 0)
                ImageFound = ImageProcessingUtils.ImageRotation(ImageFound, new Point(ImageFound.Width / 2, ImageFound.Height / 2), -Angle * Math.PI / 180.0);
            imageGB.ROI = Rectangle.Empty;
            imageGB.Dispose();
            imageGB = null;
            int count = width * height;
            int x_ok = 0;
            int y_ok = 0;
            for (int x = -X; x < X; x++)
            {
                for (int y = -Y; y < Y; y++)
                {
                    using (var temp = ImageProcessingUtils.ImageTransformation(ImageFound.Copy(), x, y))
                    {
                        CvInvoke.AbsDiff(temp, ImageRef, temp);
                        int c = CvInvoke.CountNonZero(temp);
                        if(c < count)
                        {
                            count = c;
                            x_ok = x;
                            y_ok = y;
                        }
                    }
                }
            }
            Rectangle newBound = BoudingFound;
            if (dataType == DataType.Standard)
            {
                x_ok = Convert.ToInt32(x_ok / scale);
                y_ok = Convert.ToInt32(y_ok / scale);
            }
            newBound.X += x_ok;
            newBound.Y += y_ok;
            return newBound;
        }
        public int GetPadIDOfNewPCB(int ID, int ShiftX, int ShiftY, Point CenterRotation, double Angle, DataType dataType = DataType.Display)
        {
            int newID = -1;
            Define define = new Define();
            double scale = define.DPI_Display / define.DPI_Std;
            PadItem oldPad = this.PadItems[ID];
            Point ct = new Point(oldPad.Center.X, oldPad.Center.Y);
            if(dataType == DataType.Display)
            {
                ShiftX = Convert.ToInt32(ShiftX / scale);
                ShiftY = Convert.ToInt32(ShiftY / scale);
                CenterRotation = new Point(Convert.ToInt32(CenterRotation.X / scale), Convert.ToInt32(CenterRotation.Y / scale));
            }
            ct.X += ShiftX;
            ct.Y += ShiftY;
            ct = ModelUtils.PointRotate(ct, CenterRotation, Angle * Math.PI / 180.0);
            var sorted = this.PadItems.OrderBy(item => ModelUtils.Distance(item.Center, ct));
            newID = sorted.ElementAt(0).Pad_ID;
            return newID;
        }
        public void ResetCadID(List<int> PadsID, int NewID)
        {
            foreach (var id in PadsID)
            {
                this.PadItems[id].CadFile_ID = NewID;
            }
        }
        public PCBItem DefinePCB(Rectangle Rect, DataType DataType = DataType.Display)
        {
            PCBItem pcb = new PCBItem(this.PCBItems.Count);
            Define define = new Define();
            double scale = define.DPI_Std / define.DPI_Display;
            Rectangle ROI_PCB = new Rectangle(Rect.X, Rect.Y, Rect.Width, Rect.Height);
            if (DataType == DataType.Display)
            {
                ROI_PCB.X = Convert.ToInt32(ROI_PCB.X * scale);
                ROI_PCB.Y = Convert.ToInt32(ROI_PCB.Y * scale);
                ROI_PCB.Width = Convert.ToInt32(ROI_PCB.Width * scale);
                ROI_PCB.Height = Convert.ToInt32(ROI_PCB.Height * scale);
            }
            int xmin = 65535;
            int ymin = 65535;
            int xmax = -1;
            int ymax = -1;
            for (int i = 0; i < this.PadItems.Count; i++)
            {
                var pad = this.PadItems[i];
                if (pad.Visible)
                {
                    if (ROI_PCB.Contains(pad.Center))
                    {
                        Rectangle boundPad = pad.Bouding;
                        xmin = boundPad.X < xmin ? boundPad.X : xmin;
                        ymin = boundPad.Y < ymin ? boundPad.Y : ymin;
                        xmax = boundPad.X + boundPad.Width > xmax ? boundPad.X + boundPad.Width : xmax;
                        ymax = boundPad.Y + boundPad.Height > ymax ? boundPad.Y + boundPad.Height : ymax;
                        pcb.PadItems.Add(i);
                        pad.PCB_ID = pcb.PCB_ID;
                    }
                }
            }
            if (xmin >= this.ImageWidth || ymin >= this.ImageHeight || xmax <= 0 || ymax <= 0)
            {
                pcb.PCB_ID = -1;
                return pcb;
            }
            pcb.Bounding = new Rectangle(xmin, ymin, xmax - xmin, ymax - ymin);
            return pcb;
        }
        public void RemovePCB(int PCB_ID)
        {
            for (int i = 0; i < this.PCBItems.Count; i++)
            {
                if (this.PCBItems[i].PCB_ID == PCB_ID)
                {
                    var pcb = this.PCBItems[i];
                    for (int j = 0; j < pcb.PadItems.Count; j++)
                    {
                        int idPad = pcb.PadItems[j];
                        this.PadItems[idPad].PCB_ID = -1;
                    }
                    this.PCBItems.RemoveAt(i);
                    break;
                }
            }
            for (int i = 0; i < this.PCBItems.Count; i++)
            {
                var pcb = this.PCBItems[i];
                pcb.PCB_ID = i;
                for (int j = 0; j < pcb.PadItems.Count; j++)
                {
                    int idPad = pcb.PadItems[j];
                    this.PadItems[idPad].PCB_ID = pcb.PCB_ID;
                }
            }
        }
        public void AddNewGroup(string GroupName)
        {
            this.GroupItems.Add(new GroupItem(GroupName));
        }
        public void RemoveGroup(string GroupName)
        {
            int i = 0;
            while(i < this.GroupItems.Count)
            {
                if(this.GroupItems[i].GroupName == GroupName)
                {
                    this.GroupItems.RemoveAt(i);
                }
                else
                    i++;
            }
        }
        public void UpdateItems2Group(string GroupName, List<int> IdPadItems)
        {
            for (int i = 0; i < this.GroupItems.Count; i++)
            {
                if(this.GroupItems[i].GroupName == GroupName)
                {
                    this.GroupItems[i].ListPadID.Clear();
                    this.GroupItems[i].ListPadID.AddRange(IdPadItems);
                }
            }
        }
        public bool TheSamePad(PadItem pad1, PadItem pad2)
        {
            bool theSame = false;
            double shapeScore = 1;
            using (VectorOfPoint cnt1 = new VectorOfPoint(pad1.ContourStd))
            using (VectorOfPoint cnt2 = new VectorOfPoint(pad2.ContourStd))
            {
                shapeScore = CvInvoke.MatchShapes(cnt1, cnt2, Emgu.CV.CvEnum.ContoursMatchType.I3);
            }
            if (shapeScore < 0.1)
            {
                double AreaScore = Math.Min(pad1.Area, pad2.Area) * 100.0 / Math.Max(pad1.Area, pad2.Area);
                if (AreaScore > 90)
                    theSame = true;
            }
            return theSame;
        }
        public void UncheckPad(List<int> IDPads)
        {
            foreach (var item in this.PadItems)
            {
                if (IDPads.Contains(item.Pad_ID))
                    item.Check = false;
            }
        }
        public void ActiveCheckPad(List<int> IDPads)
        {
            foreach (var item in this.PadItems)
            {
                if (IDPads.Contains(item.Pad_ID))
                    item.Check = true;
            }
        }
        public void RenderFOV(Size InFOV, double DPI, Size ImageSize, Utils.START_POINT StartPoint)
        {
            Define define = new Define();
            double scale = define.DPI_Std / DPI;
            Size FOV = new Size((int)(InFOV.Width * scale), (int)(InFOV.Height * scale));
            this.TensionItems.Clear();
            this.TensionItems = TensionItem.GetTensionPoint(this.ImageWidth, this.ImageHeight, 9, 27);
            this.FOVItems.Clear();
            foreach (var item in this.PadItems)
            {
                item.FOV_ID = -1;
            }
            using (Image<Gray, byte> image = new Image<Gray, byte>(this.ImageWidth, this.ImageHeight))
            using(VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint())
            {
                foreach (var item in this.PadItems)
                {
                    item.FOV_ID = -1;
                    bool isShow = true;
                    for (int i = 0; i < this.MarkItems.Count; i++)
                    {
                        if (this.MarkItems[i].Pad_ID == item.Pad_ID)
                        {
                            isShow = false;
                            break;
                        }
                    }
                    if (isShow && item.Check)
                    {
                        contours.Push(new VectorOfPoint(item.ContourStd));
                    }
                }
                CvInvoke.DrawContours(image, contours, -1, new MCvScalar(255), 1);
                Utils.FOVOptimizeV2 fovOptimize = new Utils.FOVOptimizeV2();
                Point[] anchors = fovOptimize.GetAnchorsFOVOptimize(image, Rectangle.Empty, FOV, StartPoint);
                for (int i = 0; i < anchors.Length; i++)
                {
                    FovItem fov = new FovItem();
                    fov.Anchor = anchors[i];
                    fov.Check = true;
                    fov.ID = i;
                    fov.ROIGerber = new Rectangle(anchors[i].X - FOV.Width / 2, anchors[i].Y - FOV.Height / 2, FOV.Width, FOV.Height);
                    fov.ROIImage = new Rectangle(ImageSize.Width / 2 - InFOV.Width / 2, ImageSize.Height / 2 - InFOV.Height / 2, InFOV.Width, InFOV.Height);
                    List<int> idMark = new List<int>();
                    foreach (var item in this.MarkItems)
                    {
                        idMark.Add(item.Pad_ID);
                    }
                    foreach (var item in this.PadItems)
                    {
                        if (fov.ID == 11 && item.Pad_ID == 4935)
                        {

                        }
                        if (fov.ROIGerber.Contains(item.Bouding) && !idMark.Contains(item.Pad_ID) && item.FOV_ID < 0)
                        {
                            
                            //if(Math.Abs(item.Bouding.X - fov.ROIGerber.X) > 30 && Math.Abs(item.Bouding.Y - fov.ROIGerber.Y) > 30 &&
                            //    Math.Abs((item.Bouding.X + item.Bouding.Width) - (fov.ROIGerber.X + fov.ROIGerber.Width)) > 30  &&
                            //    Math.Abs((item.Bouding.Y + item.Bouding.Height) - (fov.ROIGerber.Y + fov.ROIGerber.Height)) > 30
                                //)
                            {
                                item.FOV_ID = fov.ID;
                                fov.PadItems.Add(item.Pad_ID);
                            }
                        }
                    }
                    this.FOVItems.Add(fov);
                }
            }
            //List<int> padNotLink = new List<int>();
            //foreach (var item in this.PadItems)
            //{
            //    if(item.FOV_ID == -1)
            //    {
            //        padNotLink.Add(item.Pad_ID);
            //    }
            //}

        }
        public void Clear()
        {
            this.PadItems.Clear();
            this.FOVItems.Clear();
            this.PCBItems.Clear();
            this.TensionItems.Clear();
        }
    }
}
