﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Emgu.CV;
using Emgu.CV.Structure;

namespace SPI_AOI.Modelsv2
{
    public class PadItem
    {
        public int Pad_ID { get; set; }
        public double Area { get; set; }
        public bool Check { get; set; }
        public bool Visible { get; set; }
        public Hsv HsvMin { get; set; }
        public Hsv HsvMax { get; set; }
        public Rectangle Bouding { get; set; }
        public Point Center { get; set; }
        public Point CenterRotate { get; set; }
        public Point[] ContourDisplay { get; set; }
        public Point[] ContourStd { get; set; }
        public Point[] ContourStdAdjust { get; set; }
        public Point[] ContourOnFOV { get; set; }
        public PadCondition Condition { get; set; }
        public int FOV_ID { get; set; }
        public int CadFile_ID { get; set; }
        public int CadItem_ID { get; set; }
        public int PCB_ID { get; set; }
        public int ComponentPinID { get; set; }
        /// <summary>
        /// Only calculation when running
        /// </summary>
        public Point[] ContourReal { get; set; }
        public Point CenterReal { get; set; }
        public Rectangle BoudingReal { get; set; }
        public double AreaReal { get; set; }
        public PadItem()
        {
            this.Condition = new PadCondition();
            this.FOV_ID = -1;
            this.CadFile_ID = -1;
            this.CadItem_ID = -1;
            this.Check = true;
            this.PCB_ID = -1;
            this.Visible = true;
            this.HsvMin = new Hsv();
            this.HsvMax = new Hsv();
        }
    }

}
