﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Heal.Forms
{
    public partial class VisualChartForm : Form
    {
        int mGood = 0;
        int mPass = 0;
        int mNG = 0;
        public VisualChartForm(int Good, int Pass, int NG)
        {
            mGood = Good;
            mPass = Pass;
            mNG = NG;
            InitializeComponent();
            LoadUI();
        }
        private void LoadUI()
        {
            double total = mGood + mPass + mNG;
            total = total == 0 ? 1 : total;
            double rateGood = mGood * 100 / total; 
            double ratePass = mPass * 100 / total;
            double rateNG = mNG * 100 / total;
            if(rateGood == 0 && ratePass == 0 && rateNG == 0)
            {
                rateGood = 100;
            }
            chart1.Series[0].Points.AddXY("Good", rateGood);
            chart1.Series[0].Points.AddXY("Pass", ratePass);
            chart1.Series[0].Points.AddXY("NG", rateNG);
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Image Format | *.png;*jpg;*.bmp";
            saveFileDialog.DefaultExt = "png";
            saveFileDialog.AddExtension = true;
            if(saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                chart1.SaveImage(saveFileDialog.FileName, System.Windows.Forms.DataVisualization.Charting.ChartImageFormat.Png);
                System.Diagnostics.Process.Start("explorer.exe", saveFileDialog.FileName);
                this.Close();
            }
        }
    }
}
