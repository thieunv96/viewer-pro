﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson;
using MongoDB.Driver.Core;
using MongoDB.Driver;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using NLog;
using System.Threading.Tasks;

namespace Heal.Results
{
    public class PanelResult
    {
        [BsonDateTimeOptions]
        public  int No { get; set; }
        public int NumPadNG { get; set; }
        public string Side { get; set; }
        public string Tensions { get; set; }
        public int NumPCB { get; set; }
        public string PanelID { get; set; }
        public string ModelName { get; set; }
        public string PCBVersion { get; set; }
        public string PCBSide { get; set; }
        public string    LoadTime { get; set; }
        public double CycleTime { get; set; }
        public int LightWhite { get; set; }
        public int LightRed { get; set; }
        public int LightBlue { get; set; }
        public int LightGreen { get; set; }
        public int CameraExposureTime { get; set; }
        public string SN { get; set; }
        public string MachineResult { get; set; }
        public string OPResult { get; set; }
        public string[] ListPCBResult { get; set; }
        public int[] IndexSNIT { get; set; }
        public string[] ListFOVImagePath { get; set; }
        public bool Sync { get; set; }
        public PanelResult()
        {
            this.Sync = false;
        }
    }
}
