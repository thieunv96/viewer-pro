﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Attributes;
using System.Drawing;
using SPI_AOI.Modelsv2;

namespace Heal.Results
{
    public class PadResult
    {
        [BsonDateTimeOptions]
        public int No { get; set; }
        public string PanelID { get; set; }
        public string ModelName { get; set; }
        public string Time { get; set; }
        public bool Sync { get; set; }
        public string ComponentName { get; set; }
        public string ComponentCode { get; set; }
        public int ComponentPinID { get; set; }
        public int PadID { get; set; }
        public int FOVID { get; set; }
        public int PCBID { get; set; }
        public string ErrorType { get; set; }
        public string MachineResult { get; set; }
        public string OPResult { get; set; }
        public Rectangle BoudingShowPad { get; set; }
        public Point[] CntTruePad { get; set; }
        public double Area { get; set; }
        public double ShiftX { get; set; }
        public double ShiftY { get; set; }
        public PadCondition Condition { get; set; }
        public PadResult()
        {
            this.Sync = false;
        }
    }
}
