﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using SPI_AOI.Modelsv2;

namespace Heal.Processingv2
{
    public class ModelInfo
    {
        public Point[] PLCMark { get; set; }
        public Point[] GerberMark { get; set; }

        public List<FOVInfo> FovInfo = new List<FOVInfo>();
        public ModelInfo(Model model, Size FOV)
        {
            var RectFOVDisplay = model.GetFOVDiagram(FOV);
            int idM1 = model.Gerber.MarkItems[0].Pad_ID;
            int idM2 = model.Gerber.MarkItems[1].Pad_ID;
            PadItem[] padMark = new PadItem[] { model.Gerber.PadItems[idM1], model.Gerber.PadItems[idM2] };
            this.GerberMark = new Point[2];
            this.PLCMark = new Point[2];
            this.FovInfo = new List<FOVInfo>();
            for (int i = 0; i < model.Gerber.FOVItems.Count; i++)
            {
                FOVInfo fovInfo = new FOVInfo();

                fovInfo.FovItem = model.Gerber.FOVItems[i];
                fovInfo.RectDisplay = RectFOVDisplay[i];
                foreach (var padId in fovInfo.FovItem.PadItems)
                {
                    fovInfo.PadItems.Add(model.Gerber.PadItems[padId]);
                }
                this.FovInfo.Add(fovInfo);
            }
        }
        public class FOVInfo
        {
            public FovItem FovItem { get; set; }
            public List<PadItem> PadItems { get; set; }
            public Point PlcFov { get; set; }
            public System.Windows.Rect RectDisplay { get; set; }
            public FOVInfo()
            {
                this.PadItems = new List<PadItem>();
            }
        }
    }
    public class CompareImageResult
    {
        public int NumPadIssue { get; set; }
        public int NumPadCompare { get; set; }
        public VIResult VIStatus { get; set; }
        public CompareImageResult()
        {
            this.NumPadCompare = 0;
            this.NumPadIssue = 0;
            this.VIStatus = VIResult.Good;
        }
    }
    public enum VIResult
    {
        Successfully,
        CameraFailed,
        PLCFailed,
        LightSourceFailed,
        MarkPointFailed,
        MarkNotFound,
        ImagingFailed,
        GoXYFailed,
        Good,
        NG,
        ScanFailed,
        ServiceFailed,
        NotPrint,
    }
    public enum AnalyzeMode
    {
        Panel,
        PCB
    }
}
