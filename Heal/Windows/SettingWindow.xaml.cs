﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Heal.Windows
{
    /// <summary>
    /// Interaction logic for SettingWindow.xaml
    /// </summary>
    public partial class SettingWindow : Window
    {
        Properties.Settings mParam = Properties.Settings.Default;
        private bool mLoaded = false;
        public SettingWindow()
        {
            InitializeComponent();
            LoadUI();
        }
        private void LoadUI()
        {
            txtAppPath.Text = mParam.APP_CONFIG_PATH;
            txtImageHeight.Text = mParam.FOV.Height.ToString();
            txtImageWidth.Text = mParam.FOV.Width.ToString();
            mLoaded = true;
        }
        private void txtAppPath_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!mLoaded)
                return;
            TextBox txt = sender as TextBox;
            mParam.APP_CONFIG_PATH = txt.Text;
            mParam.Save();
        }

        private void txtImageWidth_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!mLoaded)
                return;
            try
            {
                TextBox txt = sender as TextBox;
                int value = Convert.ToInt32(txt.Text);
                mParam.FOV = new System.Drawing.Size(value, mParam.FOV.Height);
                mParam.Save();
            }
            catch { }
        }

        private void txtImageHeight_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!mLoaded)
                return;
            try
            {
                TextBox txt = sender as TextBox;
                int value = Convert.ToInt32(txt.Text);
                mParam.FOV = new System.Drawing.Size(mParam.FOV.Width, value);
                mParam.Save();
            }
            catch { }
        }
    }
}
