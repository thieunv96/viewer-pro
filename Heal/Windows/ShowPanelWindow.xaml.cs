﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SPI_AOI.Modelsv2;
using Heal.Results;
using System.Threading;
using System.IO;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;

namespace Heal.Windows
{
    /// <summary>
    /// Interaction logic for ShowPanelWindow.xaml
    /// </summary>
    public partial class ShowPanelWindow : Window
    {
        PanelResult mPanel = new PanelResult();
        MongoDB mMongo = MongoDB.GetInstance();
        List<PadResult> mPadResult = new List<PadResult>();
        Size mImageDiagramSize = new Size();
        Processingv2.ModelInfo mModelInfo = null;
        Model mModel = null;
        public ShowPanelWindow(PanelResult Panel)
        {
            mPanel = Panel;
            InitializeComponent();
            this.DataContext = this;
            LoadLabelPadDetail(null);
            Thread thread = new Thread(() => {
                bool RET = LoadModel(mPanel.ModelName);
                if(RET)
                {
                    var data = mMongo.QueryPad(mPanel.PanelID);
                    mPadResult.AddRange(data);
                    this.Dispatcher.Invoke(() => {
                        dgvPanelResult.ItemsSource = mPadResult;
                        dgvPanelResult.Items.Refresh();
                    });
                }
                else
                {
                    MessageBox.Show($"Not found model : \"{mPanel.ModelName}\"", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            });
            thread.Start();
        }
        private bool LoadModel(string ModelName)
        {
            mModel = Model.LoadModelByName(ModelName);
            if(mModel != null)
            {
                if(mModel.Gerber != null)
                {
                    if(mModel.Gerber.ImageWidth > 0 && mModel.Gerber.ImageHeight > 0)
                    {
                        mModelInfo = new Processingv2.ModelInfo(mModel, Properties.Settings.Default.FOV);
                        Image<Bgr, byte> imageDiagram = mModel.Getdiagram();
                        mImageDiagramSize = new Size(imageDiagram.Width, imageDiagram.Height);
                        if (imageDiagram != null)
                        {
                            this.Dispatcher.Invoke(() => {
                                var bms = Bitmap2BitmapSource(imageDiagram.Bitmap);
                                imbDiagram.Source = bms;
                                imbDiagram.UpdateLayout();
                            });
                            imageDiagram.Dispose();
                            imageDiagram = null;
                            return true;
                        }

                    }
                }
            }
            return false;
        }
        public BitmapSource Bitmap2BitmapSource(System.Drawing.Bitmap bitmap)
        {
            if (bitmap == null)
            {
                return null;
            }
            var pixelFormat = bitmap.PixelFormat;
            PixelFormat format = PixelFormats.Bgr24;
            switch (pixelFormat)
            {
                case System.Drawing.Imaging.PixelFormat.Format8bppIndexed:
                    format = PixelFormats.Gray8;
                    break;
                case System.Drawing.Imaging.PixelFormat.Format24bppRgb:
                    format = PixelFormats.Bgr24;
                    break;
                case System.Drawing.Imaging.PixelFormat.Format32bppArgb:
                    format = PixelFormats.Bgra32;
                    break;
                default:
                    break;
            }
            var bitmapData = bitmap.LockBits(
                new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height),
                System.Drawing.Imaging.ImageLockMode.ReadOnly, bitmap.PixelFormat);
            var bitmapSource = BitmapSource.Create(
                bitmapData.Width, bitmapData.Height,
                bitmap.HorizontalResolution, bitmap.VerticalResolution,
                format, null,
                bitmapData.Scan0, bitmapData.Stride * bitmapData.Height, bitmapData.Stride);
            bitmap.UnlockBits(bitmapData);
            return bitmapSource;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if(mModel != null)
            {
                mModel.Dispose();
            }
        }

        private void dgvPanelResult_PreviewMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DataGrid dataGrid = sender as DataGrid;
            int id = dataGrid.SelectedIndex;
            if(id >= 0 && id < mPadResult.Count)
            {
                PadResult pad = mPadResult[id];
                int fovID = pad.FOVID;
                string fovPath = mPanel.ListFOVImagePath[fovID];
                var loc = pad.BoudingShowPad;
                Rect rect = new Rect(loc.X, loc.Y, loc.Width, loc.Height);
                if (fovPath != null)
                {
                    if (File.Exists(fovPath))
                    {
                        using (Image<Bgr, byte> imgFOV = new Image<Bgr, byte>(fovPath))
                        {
                            BitmapSource bms = Bitmap2BitmapSource(imgFOV.Bitmap);
                            imbError.Source = bms;
                            imbError.UpdateLayout();
                            DisplayDiagram(fovID, showLine: true);
                            DisplayIssue(rect, imgFOV.Size, true);
                            using (var image = GetImagePadError(imgFOV, loc, pad.CntTruePad))
                            {
                                var bmsPad = Bitmap2BitmapSource(image.Bitmap);
                                imbPad.Source = bmsPad;
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("Không tìm thấy hình ảnh, Có thể ảnh đã quá thời gian lưu trữ!", "Not found", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                LoadLabelPadDetail(pad);
            }
            else
            {
                DisplayIssue(Rect.Empty, new System.Drawing.Size(), true);
                DisplayDiagram(-1, showLine: true);
                LoadLabelPadDetail(null);
            }
        }
        private void LoadLabelPadDetail(PadResult pad)
        {
            if(pad == null)
            {
                lbErrorType.Content = string.Empty;
                lbFOVID.Content = string.Empty;
                lbComponent.Content = string.Empty;
                lbPadID.Content = string.Empty;
                lbPCBID.Content = string.Empty;
                lbCode.Content = string.Empty;
                lbArea.Content = string.Empty;
                lbShiftY.Content = string.Empty;
                lbShiftX.Content = string.Empty;
                lbAreaLimit.Content = string.Empty;
                lbShiftXLimit.Content = string.Empty;
                lbShiftYLimit.Content = string.Empty;
                return;
            }
            string area = Math.Round(pad.Area, 1).ToString();
            area += pad.Condition.AreaLimit.Percent ? "%" : "um2";
            lbArea.Content = area;
            string shiftX = Math.Round(pad.ShiftX, 1).ToString();
            shiftX += pad.Condition.ShiftXLimit.Percent ? "%" : "um";
            lbShiftX.Content = shiftX;
            string shiftY = Math.Round(pad.ShiftY, 1).ToString();
            shiftY += pad.Condition.ShiftYLimit.Percent ? "%" : "um";
            lbShiftY.Content = shiftY;
            if (pad.Condition.AreaLimit.Percent)
                lbAreaLimit.Content = $"({Math.Round(pad.Condition.AreaLimit.LowPercent, 1)} ~ {Math.Round(pad.Condition.AreaLimit.HighPercent, 1)})";
            else
                lbAreaLimit.Content = $"({Math.Round(pad.Condition.AreaLimit.LowConst, 1)} ~ {Math.Round(pad.Condition.AreaLimit.HighConst, 1)})";
            if (pad.Condition.ShiftXLimit.Percent)
                lbShiftXLimit.Content = $"({Math.Round(pad.Condition.ShiftXLimit.LowPercent, 1)} ~ {Math.Round(pad.Condition.ShiftXLimit.HighPercent, 1)})";
            else
                lbShiftXLimit.Content = $"({Math.Round(pad.Condition.ShiftXLimit.LowConst, 1)} ~ {Math.Round(pad.Condition.ShiftXLimit.HighConst, 1)})";
            if (pad.Condition.ShiftYLimit.Percent)
                lbShiftYLimit.Content = $"({Math.Round(pad.Condition.ShiftYLimit.LowPercent, 1)} ~ {Math.Round(pad.Condition.ShiftYLimit.HighPercent, 1)})";
            else
                lbShiftYLimit.Content = $"({Math.Round(pad.Condition.ShiftYLimit.LowConst, 1)} ~ {Math.Round(pad.Condition.ShiftYLimit.HighConst, 1)})";
            lbComponent.Content = pad.ComponentName;
            lbErrorType.Content = pad.ErrorType;
            lbFOVID.Content = pad.FOVID;
            lbPadID.Content = pad.PadID;
            lbPCBID.Content = pad.PCBID;
            lbCode.Content = pad.ComponentCode;
        }
        private void DisplayDiagram(int id, bool showLine = false, int mode = 0, System.Drawing.Point locationPoint = new System.Drawing.Point())
        {
            if (id == -1)
            {
                DisplayRect(Rect.Empty, bdImbDiagram, imbDiagram, bdFOVDiagram, L1Diagram, L2Diagram, L3Diagram, L4Diagram, mImageDiagramSize, showLine);
            }
            else
            {
                DisplayRect(mModelInfo.FovInfo[id].RectDisplay, bdImbDiagram, imbDiagram, bdFOVDiagram, L1Diagram, L2Diagram, L3Diagram, L4Diagram, mImageDiagramSize, showLine, mode, locationPoint);
            }
        }
        private void DisplayIssue(Rect Recta, System.Drawing.Size size, bool showLine = false)
        {
            DisplayRect(Recta, bdError, imbError, bdPadError, L1PadError, L2PadError, L3PadError, L4PadError, new Size(size.Width, size.Height), showLine);
        }
        private void DisplayRect(Rect R, Border bd, Image imb, Border bdShow, Line nL1, Line nL2, Line nL3, Line nL4, Size realImageSize, bool ShowLine = false, int mode = 0, System.Drawing.Point padLocation = new System.Drawing.Point())
        {
            this.Dispatcher.Invoke(() => {
                if (R != null && R != Rect.Empty)
                {
                    Define define = new Define();
                    double imgWidth = realImageSize.Width;
                    double imgHeight = realImageSize.Height;
                    double imbWidth = imb.ActualWidth;
                    double imbHeight = imb.ActualHeight;
                    double bdImbWidth = bd.ActualWidth;
                    double bdImbHeight = bd.ActualHeight;


                    double scaleWidth = imbWidth / imgWidth;
                    double scaleHeight = imbHeight / imgHeight;
                    double showDisplayWidth = R.Width * scaleWidth;
                    double showDisplayHeight = R.Height * scaleHeight;

                    double stX = R.X * scaleWidth;
                    double stY = R.Y * scaleHeight;
                    stX += bdImbWidth / 2 - imbWidth / 2;
                    stY += bdImbHeight / 2 - imbHeight / 2;
                    bdShow.Width = showDisplayWidth;
                    bdShow.Height = showDisplayHeight;
                    bdShow.Margin = new Thickness(stX, stY, 0, 0);
                    bdShow.Visibility = Visibility.Visible;
                    if (ShowLine && mode == 0)
                    {
                        nL1.Visibility = Visibility.Visible;
                        nL2.Visibility = Visibility.Visible;
                        nL3.Visibility = Visibility.Visible;
                        nL4.Visibility = Visibility.Visible;
                        nL1.X1 = stX + showDisplayWidth / 2;
                        nL1.X2 = stX + showDisplayWidth / 2;
                        nL1.Y1 = bdImbHeight / 2 - imbHeight / 2;
                        nL1.Y2 = stY;

                        nL2.X1 = stX + showDisplayWidth / 2;
                        nL2.X2 = stX + showDisplayWidth / 2;
                        nL2.Y1 = stY + showDisplayHeight;
                        nL2.Y2 = bdImbHeight - (bdImbHeight / 2 - imbHeight / 2);

                        nL3.X1 = bdImbWidth / 2 - imbWidth / 2; ;
                        nL3.X2 = stX;
                        nL3.Y1 = stY + showDisplayHeight / 2;
                        nL3.Y2 = stY + showDisplayHeight / 2;

                        nL4.X1 = stX + showDisplayWidth;
                        nL4.X2 = bdImbWidth - (bdImbWidth / 2 - imbWidth / 2);
                        nL4.Y1 = stY + showDisplayHeight / 2;
                        nL4.Y2 = stY + showDisplayHeight / 2;
                    }
                    else if (padLocation != new System.Drawing.Point() && mode == 1)
                    {
                        double addWidth = bdImbWidth / 2 - imbWidth / 2;
                        double addHeight = bdImbHeight / 2 - imbHeight / 2;
                        nL1.Visibility = Visibility.Visible;
                        nL2.Visibility = Visibility.Visible;
                        nL3.Visibility = Visibility.Hidden;
                        nL4.Visibility = Visibility.Hidden;
                        nL1.X1 = padLocation.X * scaleWidth + addWidth;
                        nL1.X2 = padLocation.X * scaleWidth + addWidth;
                        nL1.Y1 = 0 + addHeight;
                        nL1.Y2 = imbHeight + addHeight;

                        nL2.X1 = 0 + addWidth;
                        nL2.X2 = imbWidth + addWidth;
                        nL2.Y1 = padLocation.Y * scaleHeight + addHeight;
                        nL2.Y2 = padLocation.Y * scaleHeight + addHeight;


                    }
                }
                else
                {
                    bdShow.Visibility = Visibility.Hidden;
                    nL1.Visibility = Visibility.Hidden;
                    nL2.Visibility = Visibility.Hidden;
                    nL3.Visibility = Visibility.Hidden;
                    nL4.Visibility = Visibility.Hidden;
                }
            });
        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            dgvPanelResult_PreviewMouseDoubleClick(dgvPanelResult, null);
        }
        public Image<Bgr, byte> GetImagePadError(Image<Bgr, byte> imageSrc, System.Drawing.Rectangle ROI, System.Drawing.Point[] Contours)
        {
            Image<Bgr, byte> imagePad = null;
            ROI.X = ROI.X < 0 ? 0 : ROI.X;
            ROI.Y = ROI.Y < 0 ? 0 : ROI.Y;
            ROI.Width = ROI.X + ROI.Width > imageSrc.Width - 1 ? imageSrc.Width - 1 - ROI.X : ROI.Width;
            ROI.Height = ROI.Y + ROI.Height > imageSrc.Height - 1 ? imageSrc.Height - 1 - ROI.Y : ROI.Height;
            System.Drawing.Point[] contours = new System.Drawing.Point[Contours.Length];

            for (int j = 0; j < contours.Length; j++)
            {
                var p = Contours[j];
                contours[j] = new System.Drawing.Point(p.X - ROI.X, p.Y - ROI.Y);
            }
            imageSrc.ROI = ROI;
            using (Image<Bgr, byte> image = imageSrc.Copy())
            {
                using (VectorOfVectorOfPoint cts = new VectorOfVectorOfPoint())
                {
                    cts.Push(new VectorOfPoint(contours));
                    CvInvoke.DrawContours(image, cts, -1, new MCvScalar(0, 255, 0), 1);
                }
                imagePad = image.Copy();
            }
            return imagePad;
        }

    }
}
