﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Driver.Linq;
using System.Drawing;
using Newtonsoft.Json;

namespace Heal
{
    public class MongoDB
    {
        private MongoClient mClient;
        private IMongoDatabase mDataBase;
        private IMongoCollection<BsonDocument> mPanelResultCollection;
        private IMongoCollection<BsonDocument> mPadIssueCollection;
        private IMongoCollection<BsonDocument> mMachineIssueCollection;
        private string DBUrl = "localhost";
        private int DBPort = 27017;
        private string DBName = "SPIMachine";
        private string PanelCol = "PanelResult";
        private string PadIssueCol = "PadResult";
        private string MachineIssueCol = "MachineIssue";
        public static MongoDB mongoDB = null;
        public static MongoDB GetInstance()
        {
            if (mongoDB == null)
                mongoDB = new MongoDB();
            return mongoDB;
        }
        public int Connect()
        {
            try
            {
                mClient = new MongoClient($"mongodb://{DBUrl}:{DBPort}");
                mDataBase = mClient.GetDatabase(DBName);
                mPanelResultCollection = mDataBase.GetCollection<BsonDocument>(PanelCol);
                mPadIssueCollection = mDataBase.GetCollection<BsonDocument>(PadIssueCol);
                mMachineIssueCollection = mDataBase.GetCollection<BsonDocument>(MachineIssueCol);
            }
            catch (Exception)
            {
                return -1;
            }
            return 0;
        }
        public Results.PanelResult[] Query(string Model, string SN, DateTime start, DateTime end, bool Good, bool Pass, bool NG)
        {
            List<Results.PanelResult> panelResults = new List<Results.PanelResult>();
            var startTime = start.ToString("yyyy-MM-dd HH:mm:ss");
            var endTime = end.ToString("yyyy-MM-dd HH:mm:ss");
            var filterBuildder = Builders<BsonDocument>.Filter;
            FilterDefinition<BsonDocument> filterDefinition = filterBuildder.Empty;
            List<FilterDefinition<BsonDocument>> listCondition = new List<FilterDefinition<BsonDocument>>();
            if (!string.IsNullOrEmpty(Model))
                listCondition.Add(filterBuildder.Eq("ModelName", Model));
            if(!string.IsNullOrEmpty(SN))
                listCondition.Add(filterBuildder.Regex("SN", SN));
            listCondition.Add(filterBuildder.Gt("LoadTime", startTime));
            listCondition.Add(filterBuildder.Lte("LoadTime", endTime));
            for (int i = 0; i < listCondition.Count; i++)
            {
                filterDefinition = filterBuildder.And(filterDefinition, listCondition[i]);
            }
            lock (mPanelResultCollection)
            {
                try
                {
                    List<BsonDocument> dataQuery = new List<BsonDocument>();
                    if(Good)
                    {
                        var filter = filterBuildder.And(filterDefinition, filterBuildder.Eq("OPResult", "Good"));
                        var data= mPanelResultCollection.Find<BsonDocument>(filter).Limit(300).ToList();
                        dataQuery.AddRange(data);
                    }
                    if (Pass)
                    {
                        var filter = filterBuildder.And(filterDefinition, filterBuildder.Eq("OPResult", "Pass"));
                        var data = mPanelResultCollection.Find<BsonDocument>(filter).ToList();
                        dataQuery.AddRange(data);
                    }
                    if (NG)
                    {
                        var filter = filterBuildder.And(filterDefinition, filterBuildder.Eq("OPResult", "NG"));
                        var data = mPanelResultCollection.Find<BsonDocument>(filter).ToList();
                        dataQuery.AddRange(data);
                    }
                    for (int i = 0; i < dataQuery.Count; i++)
                    {
                        var RET = dataQuery[i];
                        Results.PanelResult result = new Results.PanelResult();
                        result.No = i + 1;
                        result.ModelName = (string)RET["ModelName"];
                        var time = RET["LoadTime"];
                        result.LoadTime = (string)time;
                        result.Side = (string)RET["PCBSide"];
                        result.CycleTime = Math.Round( (double)RET["CycleTime"], 2);
                        result.MachineResult = (string)RET["MachineResult"];
                        result.OPResult = (string)RET["OPResult"];
                        result.PanelID = (string)RET["PanelID"];
                        var fovPaths = RET["ListFOVImagePath"].AsBsonArray;
                        result.ListFOVImagePath = new string[fovPaths.Count];
                        for (int j = 0; j < fovPaths.Count; j++)
                        {
                            if(fovPaths[j] != BsonNull.Create(null))
                                result.ListFOVImagePath[j] = (string)fovPaths[j];
                        }
                         
                        result.NumPadNG = CountPadNG(result.PanelID);
                        //if (RET["StencilLocation"] is BsonNull)
                        //    result.SN = null;
                        //else
                        //    result.SN = (string)RET["StencilLocation"];

                        var ten = RET["SN"].AsBsonArray;
                        for (int s = 0; s < ten.Count; s++)
                        {
                            if (!string.IsNullOrEmpty(result.SN))
                                result.SN += ", ";
                            result.SN += $"{Convert.ToString(ten[s])}";
                        }

                        result.NumPCB = RET["ListPCBResult"].AsBsonArray.Count;
                        string PanelID = result.PanelID;
                        panelResults.Add(result);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            return panelResults.ToArray();
        }
        public Results.PadResult[] QueryPad(string PanelID)
        {
            List<Results.PadResult> padResult = new List<Results.PadResult>();
            var filterBuildder = Builders<BsonDocument>.Filter;
            var filter = filterBuildder.And(filterBuildder.Eq("MachineResult", "NG"), filterBuildder.Eq("PanelID", PanelID));

            lock (mPanelResultCollection)
            {
                try
                {
                    var dataQuery = mPadIssueCollection.Find(filter).ToList();
                    for (int i = 0; i < dataQuery.Count; i++)
                    {
                        var data = dataQuery[i];
                        var time = data["Time"];
                        data.Remove("_id");
                        data.Remove("Time");
                        string dataStr = data.ToString();
                        Results.PadResult pad =  JsonConvert.DeserializeObject<Results.PadResult>(dataStr);
                        pad.Time = (string)time;
                        pad.No = i + 1;
                        padResult.Add(pad);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            return padResult.ToArray();
        }
        public int CountPadNG(string PanelID)
        {
            int count = 0;
            var filterBuildder = Builders<BsonDocument>.Filter;

            var filter = filterBuildder.And(filterBuildder.Eq("OPResult", "NG"), filterBuildder.Eq("PanelID", PanelID));
            
            lock (mPanelResultCollection)
            {
                try
                {
                    count = Convert.ToInt32(mPanelResultCollection.CountDocuments(filter));
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            return count;

        }

    }
}
